class AddDeletedAtToSmsUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :sms_users, :deleted_at, :datetime
    add_index :sms_users, :deleted_at
  end
end
