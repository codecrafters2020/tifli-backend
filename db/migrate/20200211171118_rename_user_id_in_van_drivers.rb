class RenameUserIdInVanDrivers < ActiveRecord::Migration[5.2]
  def change
    rename_column :van_drivers, :user_id, :sms_user_id
  end
end
