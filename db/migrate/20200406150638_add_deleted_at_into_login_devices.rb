class AddDeletedAtIntoLoginDevices < ActiveRecord::Migration[5.2]
  def change
    add_column :login_devices, :deleted_at, :datetime
  end
end
