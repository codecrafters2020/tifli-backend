class AlbumSerializer < ActiveModel::Serializer
    attributes :id, :album_name, :description, :pictures, :album_privacies


    has_many :pictures
    has_many :album_privacies

    #def pictures
    #    if object
    #        object.pictures
    #    end
    #end

end