class ChangeAvatarTypeToTextInPicture < ActiveRecord::Migration[5.2]
  def change
    change_column :pictures, :avatar, 'text USING CAST(avatar AS text)'
  end
end
