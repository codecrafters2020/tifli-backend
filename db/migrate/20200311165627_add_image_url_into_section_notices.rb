class AddImageUrlIntoSectionNotices < ActiveRecord::Migration[5.2]
  def change
    add_column :section_notices, :image_url, :text
  end
end
