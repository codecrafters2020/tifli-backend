class AddColumnInParentTeacherMeetings < ActiveRecord::Migration[5.2]
  def change
    rename_column :parent_teacher_meetings, :ptm_subject_details, :subject
    add_column :parent_teacher_meetings, :description, :text
  end
end
