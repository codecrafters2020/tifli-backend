class LeaveApplicationSerializer < ActiveModel::Serializer
    attributes :id, :student, :leave_status, :subject, :leave_reason, :image_url, :start_date, :end_date

    belongs_to :student
end