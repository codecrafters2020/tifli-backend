class Api::V1::Parent::ParentTeacherMeetingCommentsController < Api::V1::ApiController
    before_action :verify_jwt_token

    def index
        @parent_teacher_meeting_comment = ParentTeacherMeetingComment.all
    end

    def new
        @parent_teacher_meeting_comment = ParentTeacherMeetingComment.new
    end


    def show
        @parent_teacher_meeting_comment = ParentTeacherMeetingComment.find(params[:id])
    end

    def show_all_comments
        @parent_teacher_meeting_comment = ParentTeacherMeetingComment.filter_by_ptm(params[:ptm_id])
        if @parent_teacher_meeting_comment
            render json:  @parent_teacher_meeting_comment
        else
            render json: {message: "no comments found", status: 400}, status: 400
        end
    end
    def create
        @parent_teacher_meeting_comment = ParentTeacherMeetingComment.create parent_teacher_meeting_comment_params

        if @parent_teacher_meeting_comment.errors.blank?
            render json: {message: "ptm comment added", ptm_comment: @parent_teacher_meeting_comment} , status: :ok

        else
            (render json: { errors: @parent_teacher_meeting_comment.errors.full_messages }, status: :bad_request)
        end
    end


    private
    def set_parent_teacher_meeting_comment
        @parent_teacher_meeting_comment = ParentTeacherMeetingComment.find(params[:id])
    end

    def parent_teacher_meeting_comment_params
        params.require(:parent_teacher_meeting_comment).permit(
            :parent_teacher_meeting_id,
            :sent_by,
            :message,
            :id
        )
    end
end