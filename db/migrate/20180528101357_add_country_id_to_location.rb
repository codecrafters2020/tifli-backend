class AddCountryIdToLocation < ActiveRecord::Migration[5.2]
  def change
    add_column :locations, :country_id, :integer
  end
end
