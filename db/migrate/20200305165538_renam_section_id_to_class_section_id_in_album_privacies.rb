class RenamSectionIdToClassSectionIdInAlbumPrivacies < ActiveRecord::Migration[5.2]
  def change
    rename_column :album_privacies, :section_id, :class_section_id
  end
end
