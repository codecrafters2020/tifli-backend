class FacultyTimetable < ApplicationRecord
    audited

    acts_as_paranoid

    belongs_to :faculty
end