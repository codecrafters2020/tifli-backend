class Admin::ChangeDestinationRequestsController < AdminController
	before_action :set_destination_link ,only: [:index]
	before_action :all_destination_requests , only: [:index , :update]
	def index
		respond_to do |format|
			format.html
			format.xlsx {
				start_date = params[:start_date].present? ? params[:start_date].to_date : 100.years.ago.to_date
				end_date = params[:end_date].present? ? params[:end_date].to_date : 100.years.from_now.to_date
				@destinations  = ChangeDestinationRequest.where(created_at: start_date..end_date)
				render xlsx: 'xlsx_change_destination_requests', filename: "xlsx_change_destination_requests.xlsx", disposition: 'attachment'
			}
		end
	end

  def update
  	@destination = ChangeDestinationRequest.find params[:id]
  	@shipment = @destination.shipment
  	old_destination = @shipment.drop_location
  	if @shipment.update_attributes(drop_location: @destination.address, drop_lat: @destination.lat, drop_lng: @destination.lng, drop_city: @destination.city, drop_building_name: @destination.drop_building_name)
  		@shipment.destination_change_notifications old_destination
			ShipmentMailer.change_destination_approved_mail(@shipment , old_destination).deliver_now
			@destination.destroy
  		redirect_to admin_change_destination_requests_path
		else
			flash[:error]  = @shipment.errors.full_messages rescue ""
  		render :index
  	end	
  end

  def destroy
  	@destination = ChangeDestinationRequest.find params[:id]
  	if @destination.destroy
  		redirect_to admin_change_destination_requests_path
  	end
  end

  def set_destination_link
    @destination_link = "active"
	end
	def all_destination_requests
		@destinations = ChangeDestinationRequest.all
	end
end
