class CreateCourseDiary < ActiveRecord::Migration[5.2]
  def change
    create_table :course_diaries do |t|
      t.string :diary_info
      t.integer :section_course_id

      t.datetime :deleted_at
      t.timestamps
    end
  end
end
