class CourseDiary < ApplicationRecord
    audited

    acts_as_paranoid

    belongs_to :section_course
    belongs_to :student, optional: true

    has_one_attached :avatar
    scope :student_diaries, -> (class_section_id, student_id) { joins(:section_course).where('section_courses.class_section_id=? OR student_id= ? ', class_section_id, student_id) }
    scope :get_specific_day_diary, -> (diary_date){where('course_diaries.created_at::date BETWEEN ? AND ?', diary_date.to_date, diary_date.to_date)}

end