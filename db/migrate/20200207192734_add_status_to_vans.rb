class AddStatusToVans < ActiveRecord::Migration[5.2]
  def change
    add_column :vans, :status, :integer
  end
end
