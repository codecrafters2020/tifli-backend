class ChangeUserIdToSmsUserIdInLoginDevices < ActiveRecord::Migration[5.2]
  def change
    rename_column :login_devices, :user_id, :sms_user_id
  end
end
