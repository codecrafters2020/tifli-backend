class Parent < ApplicationRecord
    audited

    acts_as_paranoid

    belongs_to :sms_user
    has_many :students
end