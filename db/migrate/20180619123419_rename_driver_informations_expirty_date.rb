class RenameDriverInformationsExpirtyDate < ActiveRecord::Migration[5.2]
  def change
    rename_column :driver_informations, :expirty_date, :expiry_date
    rename_column :driver_informations, :license_expirty, :license_expiry
  end
end
