class CreateSections < ActiveRecord::Migration[5.2]
  def change
    create_table :sections do |t|
      t.string :section_name

      t.datetime :deleted_at
      t.timestamps
    end
  end
end
