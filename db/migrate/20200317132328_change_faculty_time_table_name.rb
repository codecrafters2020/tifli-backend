class ChangeFacultyTimeTableName < ActiveRecord::Migration[5.2]
  def change
    rename_table :faculty_time_tables, :faculty_timetables
  end
end
