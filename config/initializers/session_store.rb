Rails.application.config.session_store :cookie_store, key: '_sms-prodigy', domain: {
  production: '.sms_prodigy.com',
  development: '.lvh.me'
}.fetch(Rails.env.to_sym, :all)