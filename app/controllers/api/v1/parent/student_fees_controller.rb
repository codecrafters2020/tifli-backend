class Api::V1::Parent::StudentFeesController < Api::V1::ApiController
    before_action :verify_jwt_token

    def index
        @student_fees = StudentFees.all
    end

    def new
        @student_fees = StudentFees.new
    end


    def show
        @student_fees = StudentFees.find(params[:id])
    end
    def show_all_fee_details
        str_arr = params[:student_id].split(',')
        int_array = str_arr.map!(&:to_i)

        @student_fees = StudentFees.filter_by_student(int_array)

        if @student_fees
            render json: @student_fees
        else
            render json: { errors: @student_fees.errors.full_messages }, status: :bad_request
        end

    end
    def create
    end


    private
    def set_student_fees
        @student_fees = StudentFees.find(params[:id])
    end
end