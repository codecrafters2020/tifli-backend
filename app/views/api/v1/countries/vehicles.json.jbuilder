json.array! @vehicles do |vehicle|
  json.id vehicle.id
  json.name vehicle.name
  if vehicle.avatar.attached?
    json.image url_for(vehicle.avatar)
  end
end
