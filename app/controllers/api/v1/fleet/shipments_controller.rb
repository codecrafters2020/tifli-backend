class Api::V1::Fleet::ShipmentsController < Api::V1::ApiController
	
	before_action :set_shipment, only: [:update, :show, :update_destination, :vehicles]

  def show
  end

	def update
    if @shipment.update(shipment_params)
      render json: {update: true, status: :ok}
    else
    	puts @shipment.errors.full_messages
      render json: { errors:  @shipment.errors.full_messages }, status: :bad_request
    end
  end

  def update_destination
    old_destination = @shipment.drop_location
    @change_destination_request = ChangeDestinationRequest.new(change_destination_request_params)
    if @change_destination_request.save
      ShipmentMailer.change_destination_mail_to_admin(@shipment, old_destination, @change_destination_request.address)
      @shipment.destination_change_request_received_notification
      render 'show'
    else
      puts @change_destination_request.errors.full_messages
      render json: { errors:  @change_destination_request.errors.full_messages }, status: :bad_request
    end
  end

	def posted
    shipments = Shipment.fleet_posted(params[:company_id].to_i,@user)
    @shipments = Kaminari.paginate_array(shipments).page(params[:page]).per(5)
  end

  def active
    @shipments = Shipment.filter_country(@user.country_id).fleet_active(params[:company_id].to_i).remove_not_interested(@user).page params[:page]
  end

  def won
    @shipments = Shipment.where(state: ["accepted","vehicle_assigned"], fleet_id: params[:company_id].to_i).page params[:page]
  end

  def ongoing
    @shipments = Shipment.where(state: ["ongoing"], fleet_id: params[:company_id].to_i).page params[:page]
  end

  def ongoing_vehicles
    @shipments = Shipment.where(state: "ongoing", fleet_id: params[:company_id].to_i)
    @shipment_vehicles = ShipmentVehicle.where("shipment_id IN (?)", @shipments.pluck(:id))
    @vehicles = @shipment_vehicles.collect(&:vehicle)
  end

  def vehicles
    shipment_vehicles = @shipment.shipment_vehicles.where.not(status: "booked")
    @vehicles = shipment_vehicles.collect(&:vehicle)
  end

  def completed
    @shipments = Shipment.where(fleet_id: params[:company_id].to_i).where('state =? OR (state=? AND cancel_by=?)', "completed",'cancel','fleet_owner').page params[:page]    
  end
  
  def quit_bid
    Shipment.find(params[:id]).bids.where(:company_id => params[:company_id]).destroy_all
    render json: { success:  'bid quit successfully' }, status: :ok
  end

  def not_interested
    @user.not_interested_shipments.create(shipment_id: params[:id])
    render json: { success:  "Shipment is marked 'Not Interested' successfully" }, status: :ok
  end

  private

    def set_shipment
      @shipment = Shipment.find(params[:id])
    end

    def shipment_params
      params.require(:shipment).permit(:cancel_reason, :cancel_by, :state_event ,:payment_option, :state, :id,:company_id, :country_id, :pickup_location, :pickup_date, :pickup_time, :loading_time, :drop_location, :unloading_time, :expected_drop_off, :vehicle_type_id, :no_of_vehicles, :cargo_description, :cargo_packing_type, :pickup_lat, :pickup_lng, :drop_lat, :drop_lng, :drop_city, :pickup_city, :location_id, :amount, vehicle_ids: [])
    end

    def change_destination_request_params
      params.require(:change_destination_request).permit(:drop_building_name, :lat, :lng, :address ,:city, :shipment_id)
    end
end
