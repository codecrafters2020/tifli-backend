class Api::V1::Fleet::VehiclesController < Api::V1::ApiController
  before_action :set_vehicle, only: [:show, :edit, :update, :destroy,:change_driver, :update_lat_lng]

  # GET /vehicles
  # GET /vehicles.json
  def index
    
    if vehicle_assignment?
      @vehicles = Vehicle.reterive_for_assignment(current_user)
    elsif vehicle_filter?
      @vehicles = Vehicle.search_by_filters(current_user,params)
    else 
      @vehicles = Vehicle.fetch_by_company(@user.company_id)
    end
    
    render "index" , status: :ok
  end

  # GET /vehicles/1
  # GET /vehicles/1.json
  def show
  end

  # GET /vehicles/new
  def new
    @vehicle = Vehicle.new
  end

  # GET /vehicles/1/edit
  def edit
  end

  def vehicle_types
    
    render json: VehicleType.where(:country_id => @user.country_id), status: :ok
  end
  # POST /vehicles
  # POST /vehicles.json
  def create
    @vehicle =  @user.company.vehicles.create(vehicle_params)
    @vehicle.errors.blank? ? ( render "create" , status: :ok ) : (render json: { errors: @vehicle.errors.full_messages }, status: :bad_request)

  end

  # PATCH/PUT /vehicles/1
  # PATCH/PUT /vehicles/1.json
  def update
    if @vehicle.update(vehicle_params)
      render "create" , status: :ok 
    else
      render json: { errors: @vehicle.errors.full_messages }, status: :bad_request
    end
  end

  def update_lat_lng
    if @vehicle.update(vehicle_params)
      render json: { message: "lat lng updated" }, status: :ok
    else
      render json: { errors: @vehicle.errors.full_messages }, status: :bad_request
    end
  end

  # DELETE /vehicles/1
  # DELETE /vehicles/1.json
  def destroy
    @vehicle.destroy
    respond_to do |format|
      format.json { head :no_content }
    end
  end

  def change_driver
    @vehicle.user_id = params[:driver_id]
    if @vehicle.save
      render json: { success: "Driver Assigned to Vehicle Successfully" }, status: :ok
    else
      render json: { errors: @vehicle.errors.full_messages }, status: :bad_request
    end
  end

  def fetch_nearby
    @vehicles = Vehicle.within(15, :origin => [params[:lat],params[:lng]])
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_vehicle
      @vehicle = Vehicle.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def vehicle_params
      params.require(:vehicle).permit(:user_id,:id, :latitude, :longitude, :registration_number, :insurance_number, :vehicle_type_id, :company_id,:expiry_date,:authorization_letter,:available, :not_available_to,:not_available_from,{documents: []},:insurance_expiry_date)
    end

    def vehicle_assignment?
      params[:assignment].present?
    end

    def vehicle_filter?
      params[:state].present? && params[:state] != 'undefined'
    end
end
