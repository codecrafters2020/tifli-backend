class AddDeletedAtInVans < ActiveRecord::Migration[5.2]
  def change
    add_column :vans, :deleted_at, :datetime
    add_index :vans, :deleted_at
  end
end
