class RenameAvatarToImageUrlInPictures < ActiveRecord::Migration[5.2]
  def change
    rename_column :pictures, :avatar, :image_url
  end
end
