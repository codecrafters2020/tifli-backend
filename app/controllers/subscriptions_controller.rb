class SubscriptionsController < ApplicationController
	

	def create
		begin
			@gibbon = gibbon.lists(list_id).members.create(body: {email_address: params[:email], status: "subscribed"})	
		rescue Gibbon::MailChimpError => e
			@error = e.detail.sub(' Use PUT to insert or update list members.','')
		end
	end
	
	private
		def gibbon
			
			Gibbon::Request.timeout = 15
			Gibbon::Request.open_timeout = 15
			# Gibbon::Request.debug = true
			Gibbon::Request.new(api_key: "#{Rails.application.secrets.mailchimp_api_key}", symbolize_keys: true)
		end

		def list_id
			Rails.application.secrets.mailchimp_list_id
		end
end 


