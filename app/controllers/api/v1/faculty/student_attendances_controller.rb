class Api::V1::Faculty::StudentAttendancesController < Api::V1::ApiController

    before_action :verify_jwt_token
    #before_action :set_student , only: [:update_van_location]
    #
    #before_action :set_shipment, only: [:update_vehicle_status,:report_problem,:payment_received]

    def index
        @student_attendance = StudentAttendance.all
    end

    def new
        @student_attendance = StudentAttendance.new
    end

    def show
        @student_attendance = StudentAttendance.find(params[:id])
    end

    def create_all

        #@student_attendance = StudentAttendance.create student_attendance_params
        #@student_attendance.errors.blank? ? ( render "attendance added" , status: :ok ) : (render json: { errors: @student_attendance.errors.full_messages }, status: :bad_request)



        if StudentAttendance.create student_attendance_params
            render json: { message: "attendance added" }, status: :ok
        else
            render json: { errors: @student_attendance.errors.full_messages }, status: :bad_request
        end

    end

    def update_attendance
        if @student_attendance.update(student_attendance_params)
            render json: { message: "attendance updated" }, status: :ok
        else
            render json: { errors: @student_attendance.errors.full_messages }, status: :bad_request
        end
    end

    #def get_student_attendance_details
    #    @student_attendance = StudentAttendance.filter_by_student(params[:student_id])
    #
    #    puts "student_attendance: #{@student_attendance}"
    #
    #    if @student_attendance
    #
    #
    #        @student_attendance_details = get_attendance_details(@student_attendance)
    #
    #        respond_to do |format|
    #            format.json { render json: {student_attendance_details: @student_attendance_details}, status: :ok }
    #        end
    #    else
    #        render json: {message: "no records of attendance for this student", status: 400}, status: 400
    #    end
    #end

    private
    def set_student_attendance
        @student_daily_details = StudentAttendance.find(params[:id])
    end

    def student_attendance_params
        params.require(:student_attendance).map do |p|
            p.permit(
                :student_id,
                :id,
                :attendance
            )
        end

        #params.require(:student_attendance).permit()
    end
    #
    #def get_attendance_details(student_attendances)
    #    @present = 0
    #    @absent = 0
    #    @leaves = 0
    #    student_attendances.each do |student_day_record|
    #        if student_day_record.attendance.absent?
    #            @absent += 1
    #        elsif student_day_record.attendance.present?
    #            @present += 1
    #        elsif student_day_record.attendance.leave?
    #            @leaves += 1
    #        end
    #    end
    #    return [@present, @absent, @leaves]
    #end

    def set_student_link
        @student_daily_details_link = "active"
    end
end