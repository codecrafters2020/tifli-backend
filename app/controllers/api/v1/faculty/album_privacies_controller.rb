class Api::V1::Faculty::AlbumPrivaciesController < Api::V1::ApiController
    before_action :verify_jwt_token

    def index
        @album_privacy = AlbumPrivacy.all
    end

    def new
        @album_privacy = AlbumPrivacy.new
    end


    def show
        @album_privacy = AlbumPrivacy.find(params[:id])
    end

    def create

        #for each class_sections create_album_privacy
        @album_privacy = AlbumPrivacy.create album_params
        @album_privacy.errors.blank? ? ( render json: {message: "album privacy added"} , status: :ok ) : (render json: { errors: @album_privacy.errors.full_messages }, status: :bad_request)

    end

    private
    def set_album
        @album_privacy = AlbumPrivacy.find(params[:id])
    end

    def album_privacy_params
        params.require(:album_privacy).permit(
            :album_id,
            :class_section_id,
            :id
        )
    end
end