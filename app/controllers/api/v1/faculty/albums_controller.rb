class Api::V1::Faculty::AlbumsController < Api::V1::ApiController
    before_action :verify_jwt_token

    def index
        @album = Album.all
    end

    def new
        @album = Album.new
    end


    def show
        @album = Album.find(params[:id])
    end
    def show_all_albums
        @album = Album.all
        if @album
            render json: @album
        else
            render json: { errors: @album.errors.full_messages }, status: :bad_request
        end
    end

    def create
        @album = Album.create album_params
        if @album.errors.blank?
            unless params[:is_public] == true
                params[:class_sections].each do |class_section|
                    @album_privicies = AlbumPrivacy.create(album_id: @album.id, class_section_id: class_section)
                end
                render json: {message: "album added", album: @album} , status: :ok
            else
                render json: {message: "album added", album: @album} , status: :ok
            end
        else
            (render json: { errors: @album.errors.full_messages }, status: :bad_request)
        end

    end

    def delete_album
        
        @album = Album.find_by_id(params[:album_id])
        if @album
            @album.update_attributes(deleted_at: params[:current_time])

            #@album_privicies = AlbumPrivacy.find_by_album_id(params[:album_id]).all
            @album_privicies = AlbumPrivacy.filter_by_album(params[:album_id])
            if @album_privicies
                @album_privicies.update_all(deleted_at: params[:current_time])
            end

            @picture = Picture.filter_by_album(params[:album_id])
            @picture.update_all(deleted_at: params[:current_time])

            render json: {message: "album deleted"} , status: :ok
        else
            render json: {message: "album not found", status: 404} , status: 404
        end
    end

    private
    def set_album
        @album = Album.find(params[:id])
    end

    def album_params
        params.require(:album).permit(
            :album_name,
            :description,
            :is_public,
            :id
        )
    end
end