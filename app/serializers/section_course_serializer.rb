class SectionCourseSerializer < ActiveModel::Serializer
    attributes :id , :class_section, :course, :course_teacher, :students

    belongs_to :class_section
    belongs_to :course

    def course_teacher
        if object && object.faculty
            object.faculty
        end
    end

    def students
        if object && object.class_section
            object.class_section.students
        end
    end
end