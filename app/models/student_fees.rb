class StudentFees < ApplicationRecord
    audited

    extend Enumerize
    acts_as_paranoid

    belongs_to :student

    scope :filter_by_student, -> (student_id) { where('student_id IN (?)', student_id) }

    enumerize :payment_status, in: {
        :paid => 1,
        :pending => 2
    }
end