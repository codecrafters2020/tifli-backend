class Api::V1::InvoicesController < Api::V1::ApiController
	before_action :verify_jwt_token

	def send_in_email
		# binding.pry
	  if !@user.email.blank?
	    InvoiceMailer.send_in_email(@user,params[:id]).deliver_now
	    render json: {success: "Invoice successfully sent in email"}
	  else
	    render json: {error: "You don't have an email attached to your account"}
	  end
	  
	end

	
	private
	def set_shipment
	  @shipment = Shipment.find(params[:id])
	end
end
