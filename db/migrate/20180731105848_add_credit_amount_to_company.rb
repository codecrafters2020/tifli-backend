class AddCreditAmountToCompany < ActiveRecord::Migration[5.2]
  def change
    add_column :companies, :credit_amount, :float , default: 0.0
  end
end
