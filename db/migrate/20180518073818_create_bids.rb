class CreateBids < ActiveRecord::Migration[5.2]
  def change
    create_table :bids do |t|
      t.integer :company_id
      t.float :amount
      t.integer :status
      t.integer :detention
      t.integer :shipment_id
      t.timestamps
    end
  end
end
