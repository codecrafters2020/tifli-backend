class CreateCompanies < ActiveRecord::Migration[5.2]
  def change
    create_table :companies do |t|
      t.integer :user_id
      t.string :company_type
      t.string :category

      t.timestamps
    end
    add_index :companies, :user_id
  end
end
