class Api::V1::Faculty::CourseDiariesController < Api::V1::ApiController
    before_action :verify_jwt_token

    def index
        @course_diary = CourseDiary.all
    end

    def new
        @course_diary = CourseDiary.new
    end


    def show
        @course_diary = CourseDiary.find(params[:id])
    end

    def create

        @course_diary = CourseDiary.create course_diary_params
        @course_diary.errors.blank? ? ( render json: {message: "diary added"} , status: :ok ) : (render json: { errors: @course_diary.errors.full_messages }, status: :bad_request)

    end

    private
    def set_course_diary
        @course_diary = CourseDiary.find(params[:id])
    end

    def course_diary_params
        params.require(:course_diary).permit(
            :section_course_id,
            :student_id,
            :diary_info,
            :id,
            :image_url
        )
    end
end