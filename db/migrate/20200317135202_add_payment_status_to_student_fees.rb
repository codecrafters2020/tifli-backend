class AddPaymentStatusToStudentFees < ActiveRecord::Migration[5.2]
  def change
    add_column :student_fees, :payment_status, :integer
  end
end
