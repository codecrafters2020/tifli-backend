class Student < ApplicationRecord
    audited

    acts_as_paranoid

    belongs_to :class_section
    belongs_to :parent
    has_many :student_daily_details
    has_many :course_diaries
    belongs_to :van
    has_many :student_fees
    has_many :student_attendances
    has_many :leave_applications


    extend Enumerize
    #include PhoneVerificationConcern
    scope :filter_by_class_section, ->(class_section_id){where("class_section_id = ?",class_section_id)}

    enumerize :gender, in: {
        :male => 1,
        :female => 2
    }, scope: true, predicates: true
end