class CreateLeaveApplication < ActiveRecord::Migration[5.2]
  def change
    create_table :leave_applications do |t|
      t.integer :student_id
      t.string :leave_reason
      t.integer :leave_status
      t.string :remarks
      t.date :start_date
      t.text :end_date

      t.datetime :deleted_at
      t.timestamps
    end
  end
end
