class AddFacultyIdIntoPictures < ActiveRecord::Migration[5.2]
  def change
    add_column :pictures, :faculty_id, :integer
  end
end
