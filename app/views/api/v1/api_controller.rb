class Api::V1::ApiController < ApplicationController
  def validate_user
    begin
      @user = User.where("email = ? and auth_token = ?",request.headers["YOUR-EMAIL"],request.headers["YOUR-TOKEN"]).first
      raise "Invalid User" if @user.nil?
    rescue
      render json: {error: 'Invalid user'}, status: :bad_request
    end

  end
end