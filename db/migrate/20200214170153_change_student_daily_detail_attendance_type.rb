class ChangeStudentDailyDetailAttendanceType < ActiveRecord::Migration[5.2]
  def change
    remove_column :student_daily_details, :attendance , :boolean
    add_column :student_daily_details, :attendance, :integer, :default =>2
  end
end
