class Api::V1::Faculty::ClassSectionsController < Api::V1::ApiController
    before_action :verify_jwt_token

    def index
        @class_section = ClassSection.all
    end

    def new
        @class_section = ClassSection.new
    end


    def show
        @class_section = ClassSection.find(params[:id])
    end

    def show_all
        @class_section = ClassSection.all
        render json: @class_section
    end

    def create

    end


    private
    def set_class_section
        @class_section = ClassSection.find(params[:id])
    end

end