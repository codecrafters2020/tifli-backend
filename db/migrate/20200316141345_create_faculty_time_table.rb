class CreateFacultyTimeTable < ActiveRecord::Migration[5.2]
  def change
    create_table :faculty_time_tables do |t|
      t.integer :faculty_id
      t.text :image_url

      t.datetime :deleted_at
      t.timestamps
    end
  end
end
