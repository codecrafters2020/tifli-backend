class RenameUserIdInFaculty < ActiveRecord::Migration[5.2]
  def change
    rename_column :faculties, :user_id, :sms_user_id
  end
end
