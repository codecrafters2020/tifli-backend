class AddUserIdIntoLoginDevices < ActiveRecord::Migration[5.2]
  def change
    add_column :login_devices, :user_id, :integer
  end
end
