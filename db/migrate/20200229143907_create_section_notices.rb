class CreateSectionNotices < ActiveRecord::Migration[5.2]
  def change
    create_table :section_notices do |t|
      t.string :notice_info
      t.integer :class_section_id

      t.datetime :deleted_at
      t.timestamps
    end
  end
end
