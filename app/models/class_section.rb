class ClassSection < ApplicationRecord
    audited

    acts_as_paranoid

    belongs_to :faculty
    belongs_to :school_class
    belongs_to :section
    has_many :students
    has_many :section_courses
    has_many :section_notice

    scope :teached_classes, -> (faculty_id) { joins(:section_courses).where('section_courses.faculty_id=?', faculty_id) }

    scope :teached_class_sections, -> (faculty_id, school_class_id) { joins(:section_courses).where('section_courses.faculty_id=? AND school_class_id=?', faculty_id, school_class_id).distinct }
end