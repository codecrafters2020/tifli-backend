class CreateIndividualInformations < ActiveRecord::Migration[5.2]
  def change
    create_table :individual_informations do |t|
      t.string :secondary_mobile_no
      t.string :landline_no
      t.string :national_id
      t.string :expiry_date
      t.boolean :news_update

      t.timestamps
    end
  end
end
