class AddPickupBuildingNameToShipment < ActiveRecord::Migration[5.2]
  def change
    add_column :shipments, :pickup_building_name, :string, default: ""
    add_column :shipments, :drop_building_name, :string, default: ""
  end
end
