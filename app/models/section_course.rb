class SectionCourse < ApplicationRecord
    audited

    acts_as_paranoid

    belongs_to :class_section
    accepts_nested_attributes_for :class_section
    belongs_to :course
    belongs_to :faculty
    has_many :course_diary

    scope :filter_faculty, -> (section_id, faculty_id) { where("faculty_id = ? AND class_section_id = ?", faculty_id, section_id) }

end