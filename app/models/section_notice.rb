class SectionNotice < ApplicationRecord
    audited

    acts_as_paranoid

    belongs_to :class_section
    belongs_to :student, optional: true

    has_one_attached :avatar

    scope :student_notices, -> (class_section_id, student_id) { where('class_section_id=? OR student_id= ? ', class_section_id, student_id) }
    scope :get_specific_day_notice, -> (notice_date){where('created_at::date BETWEEN ? AND ?', notice_date.to_date, notice_date.to_date)}

end