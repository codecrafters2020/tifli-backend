class AddDeletedAtInClassSection < ActiveRecord::Migration[5.2]
  def change
    add_column :class_sections, :deleted_at, :datetime
  end
end
