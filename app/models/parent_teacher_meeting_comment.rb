class ParentTeacherMeetingComment < ApplicationRecord
    audited
    extend Enumerize
    acts_as_paranoid

    belongs_to :parent_teacher_meeting

    scope :filter_by_ptm, ->(ptm_id){where("parent_teacher_meeting_id = ?",ptm_id)}

    enumerize :sent_by, in: {
        :parent => 1,
        :teacher => 2
    }



end