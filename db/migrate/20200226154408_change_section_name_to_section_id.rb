class ChangeSectionNameToSectionId < ActiveRecord::Migration[5.2]
  def change
    change_column :class_sections, :section_name, 'integer USING CAST(section_name AS integer)'
    rename_column :class_sections, :section_name, :section_id
  end
end