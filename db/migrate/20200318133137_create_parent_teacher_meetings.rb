class CreateParentTeacherMeetings < ActiveRecord::Migration[5.2]
  def change
    create_table :parent_teacher_meetings do |t|
      t.integer :parent_id
      t.integer :faculty_id
      t.integer :student_id
      t.integer :ptm_type
      t.string :ptm_subject_details
      t.datetime :appointment_date

      t.datetime :deleted_at
      t.timestamps
    end
  end
end
