json.shipments do
	json.array! @shipments do |shipment|
		json.extract! shipment, :id, :pickup_location, :drop_location, :pickup_date, :expected_drop_off,:state
		json.amount number_with_precision(shipment.amount.to_f, precision: 2)
		json.payment_option shipment.try(:payment_option).try(:camelcase)
		json.pickup_time shipment.pickup_time
		json.vehicle_type shipment.try(:vehicle_type).try(:name)
		json.cancel_charges shipment.cancel_penalty
		json.company_id shipment.company_id
		json.fleet_id shipment.fleet_id
	end
end
json.total_amount @shipments.where(state: "completed").sum(:amount).to_f
json.cancel_amount @shipments.where(state: "cancel").sum(:cancel_penalty).to_f