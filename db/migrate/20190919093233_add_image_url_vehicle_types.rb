class AddImageUrlVehicleTypes < ActiveRecord::Migration[5.2]
  def change
    add_column :vehicle_types, :image_url, :text
  end
end
