class VanSerializer < ActiveModel::Serializer
    attributes :id, :van_registration_number, :van_color, :status, :van_driver, :driver_mobile, :driver_name



    #belongs_to :faculty
    #belongs_to :school_class
    belongs_to :van_driver

    def driver_mobile
        if object && object.van_driver
            object.van_driver.sms_user.mobile
        end
    end


    def driver_name
        if object && object.van_driver
            object.van_driver.sms_user.first_name.concat(object.van_driver.sms_user.last_name)
        end
    end
end

