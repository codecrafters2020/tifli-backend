class InvoiceMailer < ApplicationMailer
	default from: 'Lorryz <no-reply@lorryz.com>'
	add_template_helper(ApplicationHelper)

	def send_in_email(user,shipment_id)
		@shipment = Shipment.find(shipment_id)
	  attachments["Invoice_#{shipment_id}.pdf"] = WickedPdf.new.pdf_from_string(
	    render_to_string(pdf: 'Invoice', template: '/invoices/pdf.html.erb',:encoding => "utf8",layout: "pdf.html.erb")
	  )
	  mail(to: user.email, subject: "Lorryz Invoice for Shipment# #{shipment_id}", cc: "contact@lorryz.com" )
	end


end
