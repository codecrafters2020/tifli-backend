class VanDriver < ApplicationRecord
    audited

    acts_as_paranoid
    belongs_to :sms_user
    has_one :van , dependent: :destroy

end