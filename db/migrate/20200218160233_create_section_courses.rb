class CreateSectionCourses < ActiveRecord::Migration[5.2]
  def change
    create_table :section_courses do |t|
      t.integer :section_id
      t.integer :faculty_id
      t.integer :course_id

      t.datetime :deleted_at
      t.timestamps
    end
  end
end
