class CreatePictures < ActiveRecord::Migration[5.2]
  def change
    create_table :pictures do |t|
      t.string :picture_name
      t.string :description
      t.json :avatar

      t.integer :album_id

      t.datetime :deleted_at
      t.timestamps
    end
  end
end
