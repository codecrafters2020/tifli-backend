class CreateParents < ActiveRecord::Migration[5.2]
  def change
    create_table :parents do |t|
      t.integer :sms_user_id


      t.datetime :deleted_at
      t.timestamps
    end
  end
end
