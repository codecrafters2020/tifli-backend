class AddSectionInStudents < ActiveRecord::Migration[5.2]
  def change
    add_column :students, :class_section_id, :integer
  end
end
