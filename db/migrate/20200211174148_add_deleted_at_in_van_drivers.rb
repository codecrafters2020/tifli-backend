class AddDeletedAtInVanDrivers < ActiveRecord::Migration[5.2]
  def change
    add_column :van_drivers, :deleted_at, :datetime
    add_index :van_drivers, :deleted_at
  end
end
