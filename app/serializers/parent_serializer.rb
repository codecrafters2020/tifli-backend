class ParentSerializer < ActiveModel::Serializer
    attributes :id, :students

    #belongs_to :faculty
    #belongs_to :school_class
    #belongs_to :section
    has_many :students
    #has_many :section_courses
end