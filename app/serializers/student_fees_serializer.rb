class StudentFeesSerializer < ActiveModel::Serializer
    attributes :id, :amount, :fees_month, :payment_status, :due_date, :student

    belongs_to :student
end