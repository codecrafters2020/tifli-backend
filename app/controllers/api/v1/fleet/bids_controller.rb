class Api::V1::Fleet::BidsController < Api::V1::ApiController
  before_action :set_bid, only: [:show, :edit, :update, :destroy]

  # GET /bids
  # GET /bids.json
  def index
    @bids = shipment.available_bids
  end

  # GET /bids/1
  # GET /bids/1.json
  def show
  end

  # GET /bids/new
  def new
    @bid = Bid.new
  end

  # GET /bids/1/edit
  def edit
  end

  # POST /bids
  # POST /bids.json
  def create
    Shipment.find(params[:shipment_id]).bids.where(:company_id =>  params[:company_id]).delete_all
    @bid = shipment.bids.new(bid_params.merge(company_id: params[:company_id]))
    @bid.save
    @bid.errors.blank? ? ( render "create" , status: :ok ) : (render json: { errors: @bid.errors.full_messages }, status: :bad_request)
    
  end

  # PATCH/PUT /bids/1
  # PATCH/PUT /bids/1.json
  def update
    respond_to do |format|
      if @bid.update(bid_params)
        format.html { redirect_to @bid, notice: 'Bid was successfully updated.' }
        format.json { render :show, status: :ok, location: @bid }
      else
        format.html { render :edit }
        format.json { render json: @bid.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /bids/1
  # DELETE /bids/1.json
  def destroy
    @bid.destroy
    respond_to do |format|
      format.html { redirect_to bids_url, notice: 'Bid was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_bid
      @bid = Bid.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def bid_params
      params.require(:bid).permit(:company_id, :amount, :status, :detention, :shipment_id)
    end

    def company 
      company = Company.find(params[:company_id])
    end
    
    def shipment
      Shipment.find_by_id(params[:shipment_id])
    end
end
