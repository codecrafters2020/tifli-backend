class AddSubjectToLeaveApplication < ActiveRecord::Migration[5.2]
  def change
    add_column :leave_applications, :subject, :text
  end
end
