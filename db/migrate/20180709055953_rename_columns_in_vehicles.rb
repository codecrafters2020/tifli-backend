class RenameColumnsInVehicles < ActiveRecord::Migration[5.2]
  def change
  	rename_column :vehicles, :not_availabile_to, :not_available_to
  	rename_column :vehicles, :not_availabile_from, :not_available_from
  end
end
