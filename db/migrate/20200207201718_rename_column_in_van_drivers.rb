class RenameColumnInVanDrivers < ActiveRecord::Migration[5.2]
  def change
    rename_column :van_drivers, :"NIC", :nic
  end
end
