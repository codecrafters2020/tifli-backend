class Faculty < ApplicationRecord
    audited

    acts_as_paranoid

    belongs_to :sms_user
    has_one :class_section
    has_many :section_courses
    has_one :faculty_timetable

end