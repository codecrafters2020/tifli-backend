class Api::V1::Faculty::SectionNoticesController < Api::V1::ApiController
    before_action :verify_jwt_token

    def index
        @section_notice = SectionNotice.all
    end

    def new
        @section_notice = SectionNotice.new
    end


    def show
        @section_notice = SectionNotice.find(params[:id])
    end

    def get_class_section_teacher
        @class_section = ClassSection.find_by_faculty_id params[:faculty_id]
        if @class_section
            render json: @class_section, serializer: CompleteClassSectionSerializer
        else
            render json: {message: "teacher is not currently class teacher of any section", status: 400}, status: 400
        end
        #@class_section.errors.blank? ? ( render json: @class_section, serializer: CompleteClassSectionSerializer , status: :ok ) : (render json: { errors: @class_section.errors.full_messages }, status: :bad_request)
    end

    def create
        @section_notice = SectionNotice.create section_notice_params
        @section_notice.errors.blank? ? ( render json: {message: "notice added"} , status: :ok ) : (render json: { errors: @section_notice.errors.full_messages }, status: :bad_request)

    end

    private
    def set_section_notice
        @section_notice = SectionNotice.find(params[:id])
    end

    def section_notice_params
        params.require(:section_notice).permit(
            :class_section_id,
            :student_id,
            :subject,
            :notice_info,
            :id,
            :image_url
        )
    end
end