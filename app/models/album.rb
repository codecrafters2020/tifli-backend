class Album < ApplicationRecord
    audited

    acts_as_paranoid

    has_many :pictures
    has_many :album_privacies


    scope :section_albums, -> (class_sections_id) { left_outer_joins(:album_privacies).where('is_public = ? OR album_privacies.class_section_id IN (?)', true, class_sections_id) }
    scope :public_albums, -> {where('is_public = ?', true)}
end