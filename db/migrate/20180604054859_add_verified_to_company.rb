class AddVerifiedToCompany < ActiveRecord::Migration[5.2]
  def change
    add_column :companies, :verified, :boolean, default: false
  end
end
