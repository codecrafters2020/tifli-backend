class CompanyInformation < ApplicationRecord
	audited
  belongs_to :company
  after_create :blacklist_company
  # has_many_attached :attachments # , dependent: :destroy


  # mount_base64_uploader :avatar, AvatarUploader
  # mount_base64_uploaders :documents, DocumentUploader
  def complete?
    name.present? and address.present? and license_number.present? and license_expiry_date.present?
  end

  def blacklist_company
		self.company.update_column(:blacklisted, true)
	end
end
