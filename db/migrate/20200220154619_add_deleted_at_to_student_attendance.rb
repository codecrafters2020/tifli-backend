class AddDeletedAtToStudentAttendance < ActiveRecord::Migration[5.2]
  def change
    add_column :student_attendances, :deleted_at, :datetime
  end
end
