class Shipment < ApplicationRecord

  audited
  paginates_per 5
  after_validation :update_errors
  extend Enumerize
  attr_accessor :completed_by
  has_many :bids
  has_many :change_destination_requests, dependent: :destroy
  has_many :shipment_vehicles
  has_many :shipment_action_dates
  has_many :vehicles, :through => :shipment_vehicles
  has_many :drivers, through: :vehicles
  belongs_to :location, optional: true
  belongs_to :vehicle_type , optional: true
  belongs_to :country, optional: true
  belongs_to :company, optional: true
  belongs_to :fleet, foreign_key: :fleet_id, class_name: 'Company', optional: true
  scope :filter_country, -> (country_id) { where(:country_id => country_id) }
  scope :fleet_active, -> (company_id) {includes(:bids).where(bids: {company_id: company_id,status: [:open,:rejected]}, state: "posted")}

  scope :join_users, -> { joins("LEFT JOIN users on users.company_id = companies.id") }
  scope :filter_by_user, -> (user_id) { where('users.id =?',user_id) }
  scope :filter_by_fleet, -> (fleet_id) { where('shipments.fleet_id =?',fleet_id)}
  scope :filter_by_country_id, -> (country_id) { where('shipments.country_id =?',country_id)}
  scope :filter_by_state, -> (state) { where('shipments.state =?',state)}
  scope :filter_by_pickup_date, -> (date) { where('shipments.pickup_date =?',date)}

  scope :cargo_company, -> { where("companies.category=?",Company.category.find_value(:cargo).value.to_s) }
  scope :fleet_company, -> { where("companies.category=?",Company.category.find_value(:fleet).value.to_s) }
  scope :cargo_pending_payments, -> (user) { where(state: "completed", company_id: user.company_id, paid_by_cargo: false).sum(:amount)}
  scope :fleet_pending_payments, -> (user) {where("state = ? AND fleet_id = ? AND (paid_to_fleet = ? OR lorryz_comission_received =?)","completed", user.company_id, false, false).sum(:amount) }
  after_initialize :set_initial_status
  has_many :company_ratings

  default_scope { order(updated_at: :desc,created_at: :desc) }
  before_save :create_process
  after_commit :create_distance
  before_save :check_if_contratual
  
  acts_as_paranoid



  NO_OF_VEHICLES = (1..25).to_a

  CARGO_PACKING_TYPES =  ["Palletized","Containerized","Sacks","Boxes","Drums","Loose Cargo","Other"]

  FLEET_CANCEL_REASON = ["My customer canceled the shipment" , "No Bids received for this shipment", "Bids Received are expensive than market rates", "Other reason"]

	validates_presence_of :pickup_building_name, :drop_building_name, :unloading_time, :loading_time, :pickup_date, :pickup_location, :drop_location,:pickup_time,:vehicle_type_id, :no_of_vehicles,:cargo_description,:cargo_packing_type,:payment_option
  # validates_presence_of :pickup_building_name,
  #       :message => Proc.new { |error, attributes|
  #       "Pickup Location cannot be blank."
  #       }
  # validates_presence_of :drop_building_name,
  #       :message => Proc.new { |error, attributes|
  #       "Dropoff Location cannot be blank."
  #       }
	state_machine :state, initial: :posted do
    event :posted do
      transition [:initial, :accepted, :vehicle_assigned] => :posted
    end

    event :accepted do
      transition [:posted,:vehicle_assigned] => :accepted
    end

    event :vehicle_assigned do
      transition :accepted => :vehicle_assigned
    end

    event :ongoing do
      transition [:vehicle_assigned] => :ongoing
    end

    event :completed do
      transition [:ongoing, :vehicle_assigned] => :completed
    end

    event :cancel do
      transition [:accepted, :posted, :vehicle_assigned] => :cancel
    end

    state :posted do
      validate :validate_pickup_date_time, on: :create
    end

    state :accepted do

    end

    state :vehicle_assigned do
      validate { |shipment| (self.vehicle_ids.count != self.no_of_vehicles and self.vehicle_ids.count != 0) && self.errors[:vehicles] << "must be equal to #{self.no_of_vehicles}" }
    end

    state :initial
    state :vehicle_assigned
    state :ongoing
    state :completed
    state :cancel
    state :completed

    after_transition [:initial] => :posted do |shipment, transition|
      shipment.create_action_date("posted")
      shipment.shipment_posted_notifications
    end

    after_transition [:posted] => :accepted do |shipment, transition|
      NotInterestedShipment.where(shipment_id: shipment.id).delete_all
      shipment.create_action_date("accepted")
      shipment.update_shipment_payments
    end

    after_transition [:accepted] => :vehicle_assigned do |shipment, transition|
      shipment.create_action_date("vehicle_assigned")
      shipment.shipment_vehicles.update_all(status: "booked")
      if shipment.fleet.company_type == "company"
        shipment.vehicles.each do |vehicle|
          vehicle.driver.send_assigned_vehicle_notification(shipment,vehicle)
        end
      end
    end

    after_transition [:vehicle_assigned] => :ongoing do |shipment, transition|
      shipment.create_action_date("ongoing")
      shipment.generate_tacking_code
    end

    before_transition [:accepted, :posted, :vehicle_assigned] => :cancel do |shipment, transition|
      shipment.create_action_date("cancel")
      shipment.calculate_cancel_penalty
      shipment.update(invoice_date: Date.today)
      shipment.calculate_cancel_amount_tax
      shipment.cancel_shipment_notifications
    end

    before_transition accepted: :posted do |shipment, transition|
      # shipment.create_action_date("fleet_posted")
      shipment.calculate_cancel_penalty
      shipment.calculate_cancel_amount_tax
      shipment.reverse_bids
      shipment.cancel_shipment_notifications
    end

    before_transition vehicle_assigned: :posted do |shipment, transition|
      shipment.calculate_cancel_penalty
      shipment.calculate_cancel_amount_tax
      shipment.reverse_bids
      shipment.cancel_shipment_notifications
    end

    after_transition [:ongoing, :vehicle_assigned] => :completed do |shipment|
      shipment.create_action_date("complete")
      shipment.recalculate_shipment_payments
      shipment.update_shipment_vehicle_status if shipment.completed_by == "admin"
      shipment.rate_shipment_notifications
      shipment.invoice_to_fleet
    end
  end


  STATES = %w(posted accepted vehicle_assigned start loading en_route unloading completed cancel ongoing)

  def set_initial_status
    self.state ||= :initial
  end

  def display_date
    return self.invoice_date if ["completed", "cancel"].include?  self.state
    # return self.pickup_date
  end

  def open_bids
    bids.where(status: :open)
  end

  STATES.each do |state|
    scope state, -> { where(state: state) }
  end

  #it return cancel_penalty if state == cancel and amount otherwise
  def shipment_amount
    return self.cancel_penalty if self.state == "cancel"
    self.amount
  end

  def recalculate_shipment_payments
    self.update(invoice_date: Date.today)
    if self.completed?
      self.update_attributes(
        detention: recalculate_shipment_detention,
        amount: recalculate_shipment_amount - discount
      )
      update_shipment_payments
    elsif self.cancel?
      calculate_cancel_amount_tax
    end
  end

  def set_contract_fields
    if self.location_id.present?

      location = Location.find(self.location_id)
      self.pickup_location = location.pickup_location
      self.drop_location = location.drop_location
      self.loading_time = location.loading_time
      self.unloading_time= location.unloading_time
      self.vehicle_type_id= location.vehicle_type_id
      self.save

    end
  end

  def recalculate_shipment_detention
    if process == "prorate"
      actual_prorate_waiting_charges
    else
      actual_bidding_waiting_charges
    end 
  end

  def recalculate_shipment_amount
    if process == "prorate"
      actual_prorate_amount
    else
      actual_bidding_amount
    end 
  end

  def actual_waiting_charges_per_vehicle_for_invoice vehicle_id
    if process == "prorate"
      actual_waiting_charges_per_vehicle vehicle_id
    else
      actual_waiting_charges_per_vehicle_bidding vehicle_id
    end
  end

  def calculate_cancel_amount_tax
    self.update_attributes(
      cancel_penalty: discounted_cancel_penalty,
      amount_tax: tax_cal_cancel_penalty
    )
  end

  def discounted_cancel_penalty
    cancel_penalty - discount rescue 0
  end

  def update_shipment_payments
    self.update_attributes(
      lorryz_share_amount: lorryz_share_amount_cal,
      fleet_income: fleet_income_cal,
      amount_tax: tax_cal,
      lorryz_detention_share_amount: lorryz_detention_share_amount_cal,
      fleet_detention_income: fleet_detention_income_cal,
      detention_tax: detention_tax_cal
    ) 
  end

  def tax_cal_cancel_penalty
    if country.fleet_owner_income_tax and country.fleet_owner_income_tax > 0
      (country.fleet_owner_income_tax) * (discounted_cancel_penalty / 100)
    elsif country.lorryz_share_tax and country.lorryz_share_tax > 0
      (country.lorryz_share_tax) * (discounted_cancel_penalty / 100)
    else
      0
    end
  end

  def lorryz_share_amount_cal
    tax_invoice_lorryz_share_cal
  end

  def tax_invoice_lorryz_share_cal
    (country.commision * amount)/100
  end

  def non_tax_invoice_lorryz_share_cal
    if country.fleet_owner_income_tax and country.fleet_owner_income_tax > 0
      ((country.commision)/100) * (amount - tax_cal)
    elsif country.lorryz_share_tax and country.lorryz_share_tax > 0
      (((country.commision)/100) * (amount)) - tax_cal
    end
  end

  def lorryz_share_tax_amount
    if country.lorryz_share_tax and country.lorryz_share_tax > 0
      # lorryz_share_amount * (100 / (country.lorryz_share_tax + 100))
      country.lorryz_share_tax * (lorryz_share_amount) / (100 + country.lorryz_share_tax)
    end
  end

  def lorryz_share_tax_amount_on_cancel_panelty
    if country.lorryz_share_tax and country.lorryz_share_tax > 0
      country.lorryz_share_tax * (cancel_penalty) / (100 + country.lorryz_share_tax)
    end
  end

  def cancel_penalty_after_tax_deduction
    cancel_penalty - lorryz_share_tax_amount_on_cancel_panelty
  end

  def lorryz_share_after_tax_deduction
    lorryz_share_amount - lorryz_share_tax_amount
  end

  def fleet_income_cal
    tax_invoice_fleet_income_cal
  end

  def tax_invoice_fleet_income_cal
    amount - tax_invoice_lorryz_share_cal
  end

  def non_tax_invoice_fleet_income_cal
    if country.fleet_owner_income_tax and country.fleet_owner_income_tax > 0
      ((100 - country.commision)/100) * (amount - tax_cal)
    elsif country.lorryz_share_tax and country.lorryz_share_tax > 0
      ((100 - country.commision)/100) * (amount)
    end
  end

  def tax_cal
    if country.fleet_owner_income_tax and country.fleet_owner_income_tax > 0
      (country.fleet_owner_income_tax * amount) / (100 + country.fleet_owner_income_tax)
    elsif country.lorryz_share_tax and country.lorryz_share_tax > 0
      (country.lorryz_share_tax * (((country.commision)/100) * (amount))) / (100 + country.lorryz_share_tax)
    else
      0
    end
  end

  def detention_tax_cal
    val = 0
    if country.fleet_owner_income_tax and country.fleet_owner_income_tax > 0
      val = (country.fleet_owner_income_tax * detention) / (100 + country.fleet_owner_income_tax)
    elsif country.lorryz_share_tax and country.lorryz_share_tax > 0
      val = (country.lorryz_share_tax * detention) / (100 + country.lorryz_share_tax)
    end
    val > 0 ? val : 0
  end

  def lorryz_detention_share_amount_cal
    tax_invoice_lorryz_detention_share_amount_cal
  end

  def tax_invoice_lorryz_detention_share_amount_cal
    (country.commision * detention)/100
  end

  def non_tax_invoice_lorryz_detention_share_amount_cal
    val = 0
    if country.fleet_owner_income_tax and country.fleet_owner_income_tax > 0
      val = ((country.commision)/100) * (detention - detention_tax_cal)
    elsif country.lorryz_share_tax and country.lorryz_share_tax > 0
      val = (((country.commision)/100) * (detention)) - detention_tax_cal
    end
    val > 0 ? val : 0
  end

  def fleet_detention_income_cal
    tax_invoice_fleet_detention_income_cal
  end

  def tax_invoice_fleet_detention_income_cal
    detention - tax_invoice_lorryz_detention_share_amount_cal
  end

  def non_tax_invoice_fleet_detention_income_cal
    val = 0
    if country.fleet_owner_income_tax and country.fleet_owner_income_tax > 0
      val = ((100 - country.commision)/100) * (detention - detention_tax_cal)
    elsif country.lorryz_share_tax and country.lorryz_share_tax > 0
      val = ((100 - country.commision)/100) * (detention)
    end
    val > 0 ? val : 0
  end

  def create_action_date(state)
    ShipmentActionDate.create(shipment_id: self.id, state: state, performed_at: DateTime.now)
  end

  def create_process
    if posted?
      process = self.country.try(:process)
      self.process = process if process
      calculate_prorate_amount if process == "prorate"
    end
  end

  def create_distance
    self.update_column(:distance_between_endpoints, distance_between_start_and_end_point) if posted?
  end

  def check_if_contratual
    if posted? && location && amount != (location.rate * no_of_vehicles)
      amount = location.rate * no_of_vehicles
    end
  end

  def actual_prorate_amount
    total = 0
    self.shipment_vehicles.each do |sv|
      total += actual_prorate_formula_cal(sv.vehicle_id) 
    end
    total
  end

  def actual_prorate_formula_cal(vehicle_id)
    vehicle = self.vehicle_type
    distance = distance_between_start_and_end_point

    km_charges = (distance - vehicle.free_kms) * vehicle.rate_per_km
    amount = vehicle.base_rate + (km_charges > 0 ? km_charges : 0) + (actual_waiting_charges_per_vehicle(vehicle_id) > 0 ? actual_waiting_charges_per_vehicle(vehicle_id) : 0)
  end

  def distance_between_start_and_end_point
    # Haversine.distance(pickup_lat.to_f, pickup_lng.to_f, drop_lat.to_f, drop_lng.to_f).to_kilometers.round(2)
    url = "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=#{pickup_lat.to_f},#{pickup_lng.to_f}&destinations=#{drop_lat.to_f},#{drop_lng.to_f}&key=AIzaSyDP3epZDI0iuS9xOLgJErrAkfALAL2dQYQ"
    response = HTTParty.get(url)
    distance = response["rows"].first["elements"].first["distance"].blank? ? 0 : (response["rows"].first["elements"].first["distance"]["value"].to_f / 1000).round(2)
  end

  def actual_waiting_charges_per_vehicle(vehicle_id)
    vehicle = self.vehicle_type
    total = ((actual_load_unload_days(vehicle_id) * 24) - vehicle.load_unload_free_hours) * vehicle.waiting_charges    
    total > 0 ? total : 0
  end

  def calculate_prorate_amount
    amount = prorate_formula
    self.amount_per_vehicle = amount
    self.amount = amount * no_of_vehicles
    self.detention_per_vehicle = prorate_waiting_charges
    self.detention = prorate_waiting_charges * no_of_vehicles
  end

  def prorate_formula
    vehicle = self.vehicle_type
    distance = Haversine.distance(pickup_lat.to_f, pickup_lng.to_f, drop_lat.to_f, drop_lng.to_f).to_kilometers.round(2)
    
    km_charges = (distance - vehicle.free_kms) * vehicle.rate_per_km
    amount = vehicle.base_rate + (km_charges > 0 ? km_charges : 0) + (prorate_waiting_charges)
  end

  def prorate_waiting_charges
    vehicle = self.vehicle_type
    waiting_charges = ((loading_time || 0) + (unloading_time || 0) - vehicle.load_unload_free_hours) * vehicle.waiting_charges
    waiting_charges > 0 ? waiting_charges : 0
  end

  def actual_prorate_waiting_charges
    vehicle = self.vehicle_type
    waiting_charges = 0
    self.shipment_vehicles.each do |sv|
      waiting_charges += actual_waiting_charges_per_vehicle(sv.vehicle_id)
    end
    waiting_charges > 0 ? waiting_charges : 0
  end

  def actual_bidding_amount
    total = amount || 0
    self.shipment_vehicles.each do |sv|
      total += actual_waiting_charges_per_vehicle_bidding(sv.vehicle_id) 
    end
    total
  end

  def actual_bidding_waiting_charges
    vehicle = self.vehicle_type
    waiting_charges = 0
    self.shipment_vehicles.each do |sv|
      waiting_charges += actual_waiting_charges_per_vehicle_bidding(sv.vehicle_id)
    end
    waiting_charges > 0 ? waiting_charges : 0
  end

  def actual_waiting_charges_per_vehicle_bidding vehicle_id
    total = detention_days(vehicle_id) * detention_per_vehicle
    total > 0 ? total : 0
  end

  def calculate_cancel_penalty
    cancel_by == "cargo_owner" ? cargo_cancel_penalty : fleet_cancel_penalty
  end

  def cargo_cancel_penalty
    if self.posted?
      self.update_attribute(:cancel_penalty, 0)
    elsif self.accepted? || self.vehicle_assigned?
      if country.penalty_free_hours > ( pickup_time - Time.zone.now)/3600
        self.update_attribute(:cancel_penalty, vehicle_type.cargo_penalty_amount * no_of_vehicles)
      else
        self.update_attribute(:cancel_penalty, 0)
      end
    end
  end

  def cargo_cancel_penalty_charges
    vehicle_type.cargo_penalty_amount
  end

  def fleet_cancel_penalty_charges
    if country.fleet_penalty_free_hours && country.fleet_penalty_free_hours > ( pickup_time - Time.zone.now)/3600
      return vehicle_type.fleet_penalty_amount
    else
      return 0
    end
  end

  def cargo_cancel_penalty_charges_within_24hours
    vehicle_type.cargo_penalty_amount * no_of_vehicles
  end

  def fleet_cancel_penalty_charges_within_24hours
    if country.fleet_penalty_free_hours && country.fleet_penalty_free_hours > ( pickup_time - Time.zone.now)/3600
      return vehicle_type.fleet_penalty_amount * no_of_vehicles
    else
      return 0
    end
  end

  def fleet_cancel_penalty
    self.bids.delete_all
    dup_shipment = self.dup
    
    if country.fleet_penalty_free_hours > ( pickup_time - Time.zone.now)/3600
      dup_shipment.cancel_penalty = vehicle_type.fleet_penalty_amount * no_of_vehicles
    else
      dup_shipment.cancel_penalty = 0
    end

    dup_shipment.company_id = ""
    dup_shipment.state = "cancel"

    dup_shipment.save(validate: false)
    dup_shipment.create_action_date("cancel")
  end

  def reverse_bids
    accepted = bids.accepted
    rejected = bids.rejected
    accepted.update_all(status: :rejected)
    rejected.update_all(status: :open)
  end

  def self.fleet_posted(company_id,user)
    # binding.pry
    shipments = Shipment.posted.remove_not_interested(user).filter_country(user.country_id).select { |shipment|  shipment.bids.pluck(:company_id).exclude?(company_id) }
  end

  def self.remove_not_interested(user)
      where.not(id: user.not_interested_shipments.pluck(:shipment_id))
  end

  def shipment_posted_notifications
    User.fleet_owners.each do |user|
      PushNotification.send_notification(
        user.os,
        user.device_id,
        "",
        self.id,
        "New shipment posted from #{pickup_full_address} to #{drop_full_address} for #{vehicle_type.name}",
      ) if user.device_id

      Notification.create(notifiable_id: self.id, 
                      notifiable_type: "Shipment", 
                      body: "New shipment posted from #{pickup_full_address} to #{drop_full_address} for #{vehicle_type.name}", 
                      user_mentioned_id: user.id)
    end
  end

  def destination_change_request_received_notification
    # Cargo Notification 
    user = self.company.cargo_owner
    # dispathcer = SendSMS.new
    # message = dispathcer.message(user.mobile,"Admin will contact you shortly against your change in Shipment Destination")
    Notification.create(notifiable_id: self.id, 
                      notifiable_type: "Shipment", 
                      body: "Admin will contact you shortly against your change in Shipment Destination", 
                      user_mentioned_id: user.id)
    PushNotification.send_notification(
     user.os,
     user.device_id,
     "",
     self.id,
     "Admin will contact you shortly against your change in Shipment Destination",
    ) if user.device_id
  end

  def destination_change_notifications(old_destination)
    # Fleet Notification 
    user = self.fleet.fleet_owner
    # dispathcer = SendSMS.new
    # message = dispathcer.message(user.mobile,"The destination of shipment # #{ self.id } has been changed from #{old_destination} to #{self.drop_location}")
    Notification.create(
      notifiable_id: self.id, 
      notifiable_type: "Shipment", 
      body: "The destination of shipment # #{ self.id } has been changed from #{old_destination} to #{self.drop_location}", 
      user_mentioned_id: user.id)
    PushNotification.send_notification(
     user.os,
     user.device_id,
     "",
     self.id,
     "The destination for shipment # #{ self.id} has been changed from #{old_destination} to #{self.drop_location}",
    ) if user.device_id

    # Notification To Driver
    vehicles = self.vehicles
    vehicles.each do |vehicle|
      user = vehicle.driver
      dispathcer = SendSMS.new
      message = dispathcer.message(user.mobile,"The destination of shipment # #{ self.id } has been changed from #{old_destination} to #{self.drop_location}")
      Notification.create(
        notifiable_id: self.id, 
        notifiable_type: "Shipment", 
        body: "The destination of shipment # #{ self.id } has been changed from #{old_destination} to #{self.drop_location}", 
        user_mentioned_id: user.id)
      PushNotification.send_notification(
       user.os,
       user.device_id,
       "",
       self.id,
       "The destination for shipment # #{ self.id } has been changed from #{old_destination} to #{self.drop_location}",
      ) if user.device_id
    end
  end

  def notify_fleet_to_assign_vehicle
    user = fleet.fleet_owner
    Notification.create(
        notifiable_id: self.id, 
        notifiable_type: "Shipment", 
        body: "Reminder! Please assign a vehicle to shipment # #{ self.id }", 
        user_mentioned_id: user.id)
    PushNotification.send_notification(
     user.os,
     user.device_id,
     "",
     self.id,
     "Reminder! Please assign a vehicle to shipment # #{ self.id }",
    ) if user.device_id
  end

  def notify_driver_for_shipment_reminder
    vehicles = self.vehicles
    vehicles.each do |vehicle|
      user = vehicle.driver
      # dispathcer = SendSMS.new
      # message = dispathcer.message(user.mobile,"Reminder! Don't forget to pick shipment # #{ self.id } on #{self.pickup_time}")
      Notification.create(
        notifiable_id: self.id, 
        notifiable_type: "Shipment", 
        body: "Reminder! Don't forget to pick shipment # #{ self.id } on #{self.pickup_time}", 
        user_mentioned_id: user.id)
      PushNotification.send_notification(
       user.os,
       user.device_id,
       "",
       self.id,
       "Reminder! Don't forget to pick shipment # #{ self.id } on #{self.pickup_time}",
      ) if user.device_id
    end
  end

  def calculate_distance(lat,lng)
    url = "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=#{lat.to_f},#{lng.to_f}&destinations=#{drop_lat.to_f},#{drop_lng.to_f}&key=AIzaSyDP3epZDI0iuS9xOLgJErrAkfALAL2dQYQ"
    response = HTTParty.get(url)
    distance = response["rows"].first["elements"].first["distance"]["value"].to_f / 1000
  end
  
  def generate_tacking_code
    self.update_attribute(:tracking_code, SecureRandom.hex)
  end

  def actual_loading_days_cal vehicle_id
    loading = shipment_action_dates.where(state: "loading", vehicle_id: vehicle_id).last
    enroute = shipment_action_dates.where(state: "enroute", vehicle_id: vehicle_id).last
    (enroute.performed_at - loading.performed_at)/86400 rescue 0
  end

  def actual_unloading_days_cal vehicle_id
    unloading = shipment_action_dates.where(state: "unloading", vehicle_id: vehicle_id).last
    finish = shipment_action_dates.where(state: "finish", vehicle_id: vehicle_id).last
    (finish.performed_at - unloading.performed_at)/86400 rescue 0
  end

  def cancel_date
    shipment_action_dates.where(state: "cancel").last.try(:performed_at) rescue 0
  end

  def completed_date
    shipment_action_dates.where(state: "complete").last.try(:performed_at) rescue 0
  end

  def actual_load_unload_days(vehicle_id)
    (actual_loading_days_cal(vehicle_id) + actual_unloading_days_cal(vehicle_id))  
  end

  def load_unload_days
    ((loading_time || 0) + (unloading_time || 0))/24  
  end

  def detention_days vehicle_id
    actual_load_unload_days(vehicle_id) - load_unload_days
  end

  def self.search(query)
    return all if query_blank?(query)

    result = joins(:company).includes(:bids).join_users
    result = result.filter_by_user(query["cargo_owner"]) if query["cargo_owner"].present?
    result = result.filter_by_fleet(query["fleet_owner"]) if query["fleet_owner"].present?
    result = result.filter_by_country_id(query["country_id"])  if query["country_id"].present?
    result = result.filter_by_state(query["status"])  if query["status"].present?
    result = result.filter_by_pickup_date(query["pickup_date"])  if query["pickup_date"].present?
    
    result 
  end

  def self.query_blank?(query)
    query.values.reject(&:empty?).length == 0 
  end

  def self.states
    _states = []
    Shipment.state_machine.states.map(&:name).each do |name|
      k = name.to_s
      v = k.capitalize.humanize
      _states.push([v,k])
    end
    _states
  end  

  def if_expire?
    bids.count == 0 && (Date.today - pickup_date).to_i >= 20
  end

  def lorryz_share_calculation_for_given_amount amount
    ((country.commision)/100) * (amount)
  end

  def freight_charges
    amount.to_f - self.try(:amount_tax).to_f - self.try(:detention).to_f + self.try(:discount).to_f
  end

  def taxable_amount
    freight_charges + self.try(:detention) - self.try(:discount)
  end

  def pickup_full_address
    if pickup_location.include?(pickup_building_name)
      pickup_location
    else
      "#{pickup_building_name} #{pickup_location}"  
    end
  end

  def drop_full_address
    if drop_location.include?(drop_building_name)
      drop_location
    else
      "#{drop_building_name} #{drop_location }"  
    end
  end

  def due_date
    if completed?
      if payment_option == "credit_period"
        (invoice_date + company.credit_period.days ).strftime("%d-%m-%Y") rescue ""
      else
        (invoice_date + 3.days).strftime("%d-%m-%Y") rescue ""
      end
    elsif cancel?
      (cancel_date + 3.days).strftime("%d-%m-%Y") rescue ""
    end
  end

  def update_shipment_vehicle_status
    shipment_vehicles.each do |sv|
      sv.update_attribute(:status, 6)
    end
  end

  def validate_pickup_date_time
    if Date.today == pickup_date
      errors.add(:pickup_time, "and date cannot be back dated date and time. Should be minimum 1 hour after current date and time") if pickup_time < Time.now.utc
    end
  end

  def cancel_shipment_notifications
    ShipmentMailer.shipment_canceled(self.id).deliver
    if cancel_by == "cargo_owner"
      user = fleet.fleet_owner rescue nil
    elsif cancel_by == "fleet_owner"
      user = company.cargo_owner
    end

    Notification.create(
      notifiable_id: self.id, 
      notifiable_type: "Shipment", 
      body: "Shipment # #{ self.id } has been cancelled", 
      user_mentioned_id: user.id) if user
    PushNotification.send_notification(
     user.os,
     user.device_id,
     "",
     self.id,
     "Shipment # #{ self.id } has been cancelled",
    ) if user && user.device_id
  end

  def invoice_to_fleet
    fleet_owner = fleet.fleet_owner
    InvoiceMailer.send_in_email(fleet_owner, self.id).deliver_now if !fleet_owner.email.blank?
  end

  def rate_shipment_notifications
    fleet_owner = fleet.fleet_owner
    cargo_owner = company.cargo_owner    

    Notification.create(
      notifiable_id: self.id, 
      notifiable_type: "Shipment", 
      body: "Shipment no #{ self.id } has been completed. Please rate the Cargo Owner", 
      user_mentioned_id: fleet_owner.id) if fleet_owner
    PushNotification.send_notification(
     fleet_owner.os,
     fleet_owner.device_id,
     "",
     self.id,
     "Shipment no #{ self.id } has been completed. Please rate the Cargo Owner",
    ) if fleet_owner && fleet_owner.device_id

    Notification.create(
      notifiable_id: self.id, 
      notifiable_type: "Shipment", 
      body: "Shipment no #{ self.id } has been completed. Please rate the Fleet Owner / Driver for the service you have received", 
      user_mentioned_id: cargo_owner.id) if cargo_owner
    PushNotification.send_notification(
     cargo_owner.os,
     cargo_owner.device_id,
     "",
     self.id,
     "Shipment no #{ self.id } has been completed. Please rate the Fleet Owner / Driver for the service you have received",
    ) if cargo_owner && cargo_owner.device_id
  end

  handle_asynchronously :rate_shipment_notifications, :run_at => Proc.new { 2.minutes.from_now }
  
  def distance_from_vehicle(vehicle_id)
    sv = vehicles.find_by(id: vehicle_id)
    distance = calculate_distance(sv.latitude,sv.longitude) rescue 0
  end

  def all_vehicles_at_destination?
    vehicle_status = shipment_vehicles.pluck(:status).compact
    
    vehicle_status.count { |e| 'finish'.include? e } == self.no_of_vehicles
  end

  def get_payment_option
    case payment_option
    when "before_pick_up"
      return "Pay at origin during shipment pickup"
    when "bank_remittance"
      return "Bank Remittance"
    when "credit_period"
      return self.company.credit_period.to_i.to_s + " days credit period"
    when "after_delivery"
      return "Pay at destination during shipment delivery"
    else
      return payment_option.humanize
    end
  end

  def update_errors
    self.errors.messages[:pickup_location_name] = self.errors.messages.delete :pickup_building_name if self.errors.messages[:pickup_building_name]
    self.errors.messages[:dropoff_location_name] = self.errors.messages.delete :drop_building_name if self.errors.messages[:drop_building_name]
    self.errors.messages[:number_of_vehicles] = self.errors.messages.delete :no_of_vehicles if self.errors.messages[:no_of_vehicles]
  end

  def payment_method
    if payment_option == "credit_period"
      "Credit #{company.credit_days} Days"
    else
      try(:payment_option).try(:camelcase)
    end
  end
end
