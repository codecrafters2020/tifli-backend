class Api::V1::Parent::AlbumsController < Api::V1::ApiController
    before_action :verify_jwt_token

    def index
        @album = Album.all
    end

    def new
        @album = Album.new
    end


    def show
        @album = Album.find(params[:id])
    end
    def show_all_albums
        str_arr = params[:class_sections_id].split(',')
        int_array = str_arr.map!(&:to_i)

        @album = Album.section_albums(int_array)

        if @album
            render json: @album
        else
            render json: { errors: @album.errors.full_messages }, status: :bad_request
        end

    end
    def create
    end


    private
    def set_album
        @album = Album.find(params[:id])
    end

    def album_params
        params.require(:album).permit(
            :album_name,
            :description,
            :is_public,
            :id
        )
    end
end