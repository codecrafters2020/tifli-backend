class AddFieldsToSmsUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :sms_users, :first_name, :string
    add_column :sms_users, :last_name, :string
    add_column :sms_users, :mobile, :string
    add_column :sms_users, :role, :integer
  end
end
