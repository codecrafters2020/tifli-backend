class AddAvatarsToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :individual_informations, :avatar, :json
    add_column :company_informations, :avatar, :json
  end
end
