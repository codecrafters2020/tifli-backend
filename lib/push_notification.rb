module PushNotification
	def self.send_notification(os, device_token, url, id, message)
		if os == "ios"
			APNS.send_notification(device_token, alert: message, badge: 0, sound: 'default', :other => {id: id, path: url, count: 0})
		elsif os == "android"
			options = { data: { message: message, path: url, count: 0, id: id}}
			Fcm.send([device_token], options)
		end	
	end
end