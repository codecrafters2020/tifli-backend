class ChangeShipmentExpectedDropOffType < ActiveRecord::Migration[5.2]
  def change
    change_column :shipments, :expected_drop_off , :date
  end
end
