class Api::V1::SmsUsersController < Api::V1::ApiController

    before_action :verify_jwt_token, except: [:reset_password]

    #def send_phone_verify_code
    #    @user.update(params.permit(:mobile)) && (@user.assgin_verification_code; @user.send_verification_code)
    #end

    #def verify_phone_number
    #    if @user.verify_code?(params[:code])
    #        render json: {verified: true}
    #    else
    #        render json: {verified: false, error: "Code is invalid"}
    #    end
    #end


    #def resent_code
    #    @user.send_verification_code
    #    if @user.mobile_verified == "code_sent"
    #        render json: {sent: true,success: "Code is sent on your Phone"}
    #    else
    #        render json: {sent: false,error: "We are unable to sent code. Please try later"}
    #    end
    #end


    #def register_as_fleet_owner
    #    if @user.update_column(:role, User.role.find_value(:fleet_owner).value)
    #        render json: {registerd_as_fleet_owner: true,success: "success"}
    #    else
    #        render json: {registerd_as_fleet_owner: true,error: "We are unable to register you as fleet owner"}
    #    end
    #end

    #def register_as_cargo_owner
    #    if @user.update_column(:role, User.role.find_value(:cargo_owner).value)
    #        render json: {registerd_as_cargo_owner: true,success: "success"}
    #    else
    #        render json: {registerd_as_cargo_owner: true,error: "We are unable to register you as fleet owner"}
    #    end
    #end

    #def register_as_individual
    #
    #    company = @user.register_as_individual
    #
    #    if company
    #        render json: {registerd_as_fleet_owner: true,success: "success"}
    #    else
    #        render json: {registerd_as_fleet_owner: true,error: "We are unable to register you as fleet owner"}
    #    end
    #end

    #def register_as_company
    #
    #    company = @user.register_as_company
    #
    #    if company
    #        render json: {registerd_as_fleet_owner: true,success: "success"}
    #    else
    #        render json: {registerd_as_fleet_owner: true,error: "We are unable to register you as fleet owner"}
    #    end
    #end

    def current_sms_user_detail
        render json: @sms_user
    end

    def reset_password
        @sms_user = User.find_by_email_or_phone(params)
        if @sms_user
            password = Devise.friendly_token(length = 8)
            @sms_user.password = password
            @sms_user.save
            @sms_user.send_reset_password_code(password)
            render json: {success: "Pasword has been sent to your phone number"}
        else
            render json: {errors: "Can't find user."}
        end
    end

    def update
        #make new call in login_devices_controller
        if @sms_user.update_attributes(user_params)
            render json: {
                status: :ok
            }
        else
            render json: {
                errors: "#{@sms_user.errors}",
                status: 400
            }
        end
    end

    def destroy
        @sms_user = SmsUser.find_by_id params[:id]
        #SmsUser.login_devices.where(device_id: params[:device_id]).delete_all

        #@sms_user.update_attributes(device_id: "")
        render json: {message: "Signout Successful"}, status: :ok
    end

    def set_login_device
        @login_device = LoginDevice.create login_device_params
        if @login_device.errors.blank?
            render json: @login_device
        else
            (render json: { errors: @login_device.errors.full_messages }, status: :bad_request)
        end
    end

    def remove_login_device
        @login_device = LoginDevice.where(device_id: params[:device_id])
        if @login_device
            @login_device.update_all(deleted_at: params[:current_time])
            render json:{message: "login device removed"}, status: :ok
        else
            render json: { message: "Not found"}, status: :ok
        end
    end
    def notifications
        @sms_user = SmsUser.find params[:id]
        @sms_user.unread_notifications.update_all(viewed_flag: true)
        @notifications = @sms_user.notifications.order(created_at: :desc).page params[:page]
        render 'notifications', status: :ok
    end

    private
    def user_params
        params.require(:sms_user).permit(:id,:os, :email, :mobile, :password, :password_confirmation, :current_password)
    end

    def login_device_params
        params.require(:login_device).permit(
                :device_id,
                :os,
                :sms_user_id,
                :id
            )
    end
end
