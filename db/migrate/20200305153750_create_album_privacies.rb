class CreateAlbumPrivacies < ActiveRecord::Migration[5.2]
  def change
    create_table :album_privacies do |t|
      t.json :avatar

      t.integer :album_id
      t.integer :section_id

      t.datetime :deleted_at
      t.timestamps
    end
  end
end
