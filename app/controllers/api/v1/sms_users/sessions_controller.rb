class Api::V1::SmsUsers::SessionsController < Devise::SessionsController

  require 'auth_token'
  include SmsUserConcern
  # Disable CSRF protection
  skip_before_action :verify_authenticity_token
  skip_before_action :verify_signed_out_user
  # Be sure to enable JSON.
  respond_to :html,:json
  
  # POST /resource/sign_in
  def create
    #make_phone_number params
    @sms_user = SmsUser.find_by_email_or_phone(params)
    unless @sms_user
      return render json: {error: 'Invalid credentialssssss'}, status: :unauthorized
    end

    if email_authentication?(params)
        if @sms_user && @sms_user.valid_password?(params[:sms_user][:password])
                # debugger
                # warden.set_user(@user, scope: :user)
                # self.resource = warden.authenticate!(auth_options)
                # set_flash_message(:notice, :signed_in) if is_flashinmobileg_format?
                sign_in(@sms_user, store: false)
                # yield resource if block_given?
                render json: @sms_user
        else
            render json: {error: 'Invalid Phone no / password '}, status: :unauthorized
        end
    elsif phone_authentication?(params)
        if @sms_user && @sms_user.valid_password?(params[:sms_user][:password])
            render json: @sms_user
        else
            render json: {error: 'Invalid Phone no / password ppppp'}, status: :unauthorized
        end
    else
        render json: {error: 'Email is invalid'}, status: :unauthorized
    end
  end

  def destroy
    unless all_signed_out?
      Devise.sign_out_all_scopes ? sign_out : sign_out(resource_name)
      yield if block_given?
      render body: 'Logout Successfully', status: json_status(true)
    else
      render body: 'User Already Logged out', status: json_status(true)
    end
  end

  def require_no_authentication
  end

  private

  def make_phone_number params
    dialing_code = Country.find_by_dialing_code(params[:sms_user][:country]).try(:dialing_code)
    
    # if dialing_code.present?
    #   params[:user][:email] = dialing_code + params[:user][:email].strip
    # end
  end

end
