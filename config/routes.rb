Rails.application.routes.draw do

  #devise_for :sms_users
  resources :login_devices
  get 'activities/index'
  match '*path', via: [:options], to: lambda {|_| [204, { 'Content-Type' => 'text/plain' }]}
  resources :track do
    member do
      get :get_coordinates
    end
  end
  namespace :admin do
    root 'dashboard#index'
    devise_scope :user do
      get '/sign_in' => "/users/sessions#new" , as: :sign_in
      get '/sign_up' => "/users/registrations#new", as: :sign_up
    end
    devise_scope :sms_user do
      get '/sign_in' => "/sms_users/sessions#new" , as: :sms_sign_in
      get '/sign_up' => "/sms_users/registrations#new", as: :sms_sign_up
    end
    get "/dashboard/cargo-owners" => "dashboard#cargo_owners" , as: :cargo_owners
    get "/dashboard/fleet-owners" => "dashboard#fleet_owners" , as: :fleet_owners
    post "/dashboard/mark-verified/:company_id" => "dashboard#mark_verified", as: :mark_verified
    post "/dashboard/mark-featured/:company_id" => "dashboard#mark_featured", as: :mark_featured
    post "/dashboard/mark-blacklisted/:company_id" => "dashboard#mark_blacklisted", as: :mark_blacklisted
    post "/dashboard/tax_invoice/:company_id" => "dashboard#tax_invoice", as: :tax_invoice
    post "/dashboard/mark-safe-for-cash-on-delivery/:company_id" => "dashboard#mark_safe_for_cash_on_delivery", as: :mark_safe_for_cash_on_delivery
    get "/dashboard/show-user/:user_id" => "dashboard#show_user", as: :show_user
    get "/dashboard/edit-user/:user_id" => "dashboard#edit_user", as: :edit_user
    patch "/dashboard/update-user/:user_id" => "dashboard#update_user", as: :update_user
    get "/dashboard/cargo-contracts/:company_id" => "dashboard#cargo_contracts", as: :cargo_contracts
    post "/payments/mark-paid-by-cargo/:shipment_id" => "payments#mark_paid_by_cargo", as: :mark_paid_by_cargo
    post "/payments/mark-lorryz-comission-received/:shipment_id" => "payments#mark_lorryz_comission_received", as: :mark_lorryz_comission_received
    post "/payments/mark-paid-to-fleet/:shipment_id" => "payments#mark_paid_to_fleet", as: :mark_paid_to_fleet
    post "/payments/cancel_penalty_amount_received/:shipment_id" => "payments#cancel_penalty_amount_received", as: :cancel_penalty_amount_received

    resources :pages
    resources :invoices , only: [:show]
    resources :countries , except: [:destroy]
    resources :vehicle_types, except: [:destroy]
    resources :locations
    resources :change_destination_requests
    resources :shipments do 
      member do 
        delete :cancel
      end
      collection do
        post :get_bulk_coordinates
      end
    end
    resources :users
    resources :payments
  end
  namespace :cargo do
    root 'dashboard#index'
    resources :payments
    resources :shipments do
      collection do
        get :posted
        get :ongoing
        post :calculate_fare
        get :upcoming
        get :completed
        post :get_bulk_coordinates
      end
      member do
        get :get_coordinates
        get :cancel
        get :update_destination
        patch :update_destination
      end
    end
    resources :bids do
      member do
        post :accept
        post :reject
      end
    end
    resources :dashboard , only: [:index]
  end
  namespace :fleet do
    root 'shipments#posted'
    resources :payments
    resources :shipments do
      collection do
        get :posted
        get :active
        get :ongoing
        get :won
        get :completed
      end
      member do
        get :quit_bid
        get :assign_vehicle
        get :cancel
        get :get_coordinates
        post :not_interested
      end
      resources :bids do
        collection do
          post :bid_details
          get :bidNow
          # get :notInterested
        end
      end
    end
    resources :drivers do
      collection do
        post :register_as_driver
      end
    end
    resources :vehicles do
      collection do
        get :vehicle_types
      end
      member do
        post :change_driver
        get :change_driver
      end
    end

    resources :dashboard , only: [:index]
  end
  resources :countries do
    collection do
      get "location/:location_id" => "countries#location"
      get :get_vehicles_by_country
      # get :vehicles
    end
  end

  devise_for :users, controllers: {:invitations => 'users/invitations',passwords: 'users/passwords', sessions: 'users/sessions', registrations: 'users/registrations',confirmations: 'users/confirmations'   }

  devise_scope :user do
    # match "user/account" => "user#account", as: :user_account, via: [:get, :post]
    match '/users/mobile-verification' => 'users/registrations#mobile_verification' , via: [:get, :post]
    get '/users/send-verification-code/:user_id' => 'users/registrations#send_verification_code', as: :send_verification_code
    match '/sign_up' => "users/registrations#new", as: :sign_up,via: [:get, :post]
  end

  devise_for :sms_users, controllers: {passwords: 'sms_users/passwords', sessions: 'sms_users/sessions', registrations: 'sms_users/registrations',confirmations: 'sms_users/confirmations'}
  devise_scope :sms_user do
    # match "user/account" => "user#account", as: :user_account, via: [:get, :post]
    #match '/users/mobile-verification' => 'users/registrations#mobile_verification' , via: [:get, :post]
    #get '/sms_users/send-verification-code/:user_id' => 'sms_users/registrations#send_verification_code', as: :send_verification_code
    match '/sign_up' => "sms_users/registrations#new", as: :sms_user_sign_up,via: [:get, :post]
  end
  
  resources :companies, :only => [  ] do
    collection do
      post :add_company_information
      patch :add_company_information
      post :add_individual_information
      patch :add_individual_information
      get :edit
      get :get_individual_information
      get :get_company_information
      put :presign_upload
    end
  end
  match "/ratings" => "rating#rate", via: [:get, :post] , as: :ratings_shipment
  resources :invoices , only: [:show]
  namespace :api do
  	namespace :v1 do
      devise_for :users, controllers: { sessions: 'api/v1/users/sessions', registrations: 'api/v1/users/registrations'  }
      devise_for :sms_users, controllers: { sessions: 'api/v1/sms_users/sessions', registrations: 'api/v1/sms_users/registrations'  }

      resources :ratings do
      end
      resources :invoices do 
        member do 
          post :send_in_email
        end
      end
      namespace :cargo do
        resources :companies do
          resources :payments
          resources :shipments do
            collection do
              get :posted
              get :ongoing
              get :upcoming
              post :calculate_fare
              get :completed
            end
            resources :bids do
              member do
                post :accept
                post :reject
              end
            end
          end
        end
      end
      namespace :fleet do
        resources :drivers do
          collection do
            post :register_as_driver
            post :upcoming_shipments
            post :ongoing_shipments
            post :completed_shipments
            post :update_vehicle_status
            post :report_problem
            post :payment_received
          end
        end
        resources :vehicles do 
          collection do 
            get :vehicle_types
            post :change_driver
            get :fetch_nearby
          end
          member do
            put :update_lat_lng
          end
        end 
        resources :companies do
          resources :payments
          resources :shipments do
            collection do
              get :posted
              get :active
              get :won
              get :ongoing
              get :ongoing_vehicles
              get :completed
              
            end
            member do 
              post :quit_bid
              put :update_destination
              get :vehicles
              post :not_interested
            end
          resources :bids do
            collection do
              get :bidNow
              get :notInterested
            end
          end
          end
        end
      end

      namespace :faculty do
          resource :albums do
              get :show_all_albums
              patch :delete_album
          end

          resource :class_sections do
              get :show_all
          end

          resource :course_diaries do
          end

          resource :faculty_timetable do
            get :get_faculty_timetable
          end

          resource :leave_applications do
              get :show_all_requested_leaves
              get :get_leaves_by_date
              patch :update_leave_status
          end

          resource :parent_teacher_meetings do
            get :show_all_requested_meetings
            patch :set_appointment_date
          end
          resource :parent_teacher_meeting_comments do
            get :show_all_comments
          end

          resource :picture do
              patch :delete_pictures
          end

          resource :section_courses do
              get :get_faculty_teached_class
              get :get_faculty_teached_section
              get :get_section_courses
          end

          resource :section_notices do
              get :get_class_section_teacher
          end

          resource :student_attendance do
            post :create_all
          end
      end

      namespace :van_driver do
          resources :van_drivers do

          end
          resources :vans do
            collection do
              post :update_status
              post :update_van_location
            end
          end
      end

      namespace :parent do
          resource :albums do
              get :show_all_albums
          end

          resource :course_diaries do
              get :get_student_diaries
          end

          resource :leave_applications do
              get :show_all_requested_leaves
          end

          resource :parent_teacher_meetings do
              get :show_all_requested_meetings
          end

          resource :parent_teacher_meeting_comments do
              get :show_all_comments
          end

          resource :section_notices do
              get :get_student_notices
          end

          resource :student do
              get :get_student_attendance_details
              get :get_student_van
              get :get_van_current_location
              get :get_detailed_student_attendance
          end

          resource :student_fees do
              get :show_all_fee_details
          end
      end
      #namespace :student do
      #    resource :student_attendance do
      #        post :create_all
      #        get :get_student_attendance_details
      #    end
      #end

      resources :companies, :only => [] do
        collection do
          post :add_company_information
          post :add_individual_information        
          get :get_individual_information
          get :get_company_information
        end
      end
      resources :countries do
        member do
          get :locations
          get :vehicles
        end 
      end
      resources :signed_urls do 
        collection do
          put :presign_upload
        end
      end

      resources :events do
          get :get_all_events
      end

      resources :users, :only => [:update, :destroy] do
        collection do
          post :send_phone_verify_code
          put :verify_phone_number
          post :resent_code

          post :register_as_fleet_owner
          post :register_as_cargo_owner
          post :register_as_individual
          post :register_as_company
          post :current_user_detail
          post :reset_password
        end
        member do 
          get :notifications
        end
      end
      resources :sms_users, :only => [:update, :destroy] do
        collection do

            post :set_login_device
            post :current_user_detail
            post :reset_password
            patch :remove_login_device
        end
        member do
            get :notifications
        end
      end
    end
  end

  resources :notifications , only: [] do
    collection do
      post :mark_viewed
    end
  end
  resources :users
  resources :subscriptions
  resources :dashboard  , only: [:index] do
    collection do
      get :home
      post :home_shipment
      post :share_iphone
      post :share_android
      post :contact
      get  :privacy_policy
      get  :terms_and_conditions
    end
  end

 resources :pages
  # get "/del" => "company#del"
  root 'dashboard#home'
end


