class AddMaxAmountAndMaxDaysToCountries < ActiveRecord::Migration[5.2]
  def change
    add_column :countries, :max_days, :int
    add_column :countries, :max_amount, :float
  end
end
