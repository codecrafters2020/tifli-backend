class AlbumPrivacy < ApplicationRecord
    audited

    acts_as_paranoid

    belongs_to :album
    belongs_to :class_section

    scope :filter_by_album, -> (album_id){where('album_id = ?', album_id)}
    #scope :teached_class_sections, -> (faculty_id) { joins(:section_courses).where('section_courses.faculty_id=?', faculty_id) }

end