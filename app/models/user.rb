class User < ApplicationRecord
  audited
  # tracked owner: Proc.new{ |controller, model| controller.try(:current_user).present? ? controller.current_user : model.full_name}

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  acts_as_paranoid
  validates :email, :uniqueness => {:allow_blank => true}
  attr_writer :login
  audited
  # has_many_attached :attachments
  devise :invitable, :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,:confirmable
    validates :mobile, presence: true, uniqueness: true
    # validates :role, presence: true
    extend Enumerize
    include PhoneVerificationConcern
    enumerize :role, in: {
  												:superadmin => 1,
  												:admin => 2,
  												:cargo_owner => 3,
  												:fleet_owner => 4,
  												:cargo => 5,
  												:fleet=> 6,
                          :driver=> 7 }, scope: true, predicates: true

    belongs_to :company, optional: true
    belongs_to :country, optional: true
    has_one :driver_information
    has_one :vehicle
    scope :drivers, -> {where(role: :driver)}
    scope :fleet_owners ,-> {where(role: :fleet_owner)}
    scope :cargo_owners ,-> {where(role: :cargo_owner)}
    scope :admins ,-> {where(role: :admin)}
    scope :superadmins ,-> {where(role: :superadmin)}
    scope :filter_by_user, -> (user_id) { where('users.id =?',user_id) }
    #cargo company Shipments
    #fleet company shipments
    accepts_nested_attributes_for :company #,reject_if: proc { |attributes| attributes['title'].blank? }
    has_many :shipments, through: :company

    #driver
    has_one :vehicle
    has_many :assigned_shipments, through: :vehicle,source: :shipments
    has_many :not_interested_shipments
    has_many :notifications, class_name: "Notification", foreign_key: :user_mentioned_id
    has_many :unread_notifications, -> { where("notifications.viewed_flag = ?", false) },  class_name: "Notification", foreign_key: :user_mentioned_id

  def self.fetch_user token
      decoded_token = JWT.decode(token, Rails.application.secrets.secret_key_base)
      User.find(decoded_token[0]["user_id"])
  end

    def set_credit_period_and_amount
      if self.company.present?
        company = self.company
        if fleet_owner?
          credit_amount = country.fleet_max_amount if [0.0 , nil].include? company.credit_amount
          credit_period = country.fleet_max_days if [0.0 , nil].include? company.credit_period
          company.update(credit_amount: credit_amount ,credit_period: credit_period  )
        elsif cargo_owner?
          credit_amount = country.cargo_max_amount if [0.0 , nil].include? company.credit_amount
          credit_period = country.cargo_max_days if [0.0 , nil].include? company.credit_period
          company.update(credit_amount: credit_amount ,credit_period: credit_period  )
        end
      end
    end
    def self.find_by_email_or_phone(params)
      
      find_by(mobile: params[:user][:mobile]) || find_by(email: params[:user][:email])

    end

    def self.find_by_email_or_phone_web(params)
      find_by(mobile: params[:user][:email]) || find_by(email: params[:user][:email])
    end

    def get_company_category
      (cargo? or cargo_owner?) ? Company.category.find_value(:cargo).value : Company.category.find_value(:fleet).value
    end

    def build_company_type params
      if params[:user][:company_attributes] and params[:user][:company_attributes][:company_type] == "Individual"
        register_as_individual
      elsif params[:user][:company_attributes] and params[:user][:company_attributes][:company_type] == "Company"
        register_as_company
      end
    end

    def register_as_individual
      build_company(company_type: Company.company_type.find_value(:individual).value, category: get_company_category )
      save
      set_credit_period_and_amount
    end

    def register_as_company
      build_company(company_type: Company.company_type.find_value(:company).value, category: get_company_category )
      save
      set_credit_period_and_amount
    end

    def login=(login)
      @login = self.email
    end

    def email_required?
      false
    end

    def login
      @login || self.mobile || self.email
    end

    def self.find_for_database_authentication(warden_conditions)
      conditions = warden_conditions.dup
      if login = conditions.delete(:login)
        where(conditions.to_h).where(["lower(mobile) = :value OR lower(email) = :value", { :value => login.downcase }]).first
      elsif conditions.has_key?(:mobile) || conditions.has_key?(:email)
        where(conditions.to_h).first
      end
    end

  def full_name
    "#{first_name} #{last_name}"
  end

end
