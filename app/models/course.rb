class Course < ApplicationRecord
    audited

    acts_as_paranoid
    has_many :section_courses

end