class Api::V1::Parent::ParentTeacherMeetingsController < Api::V1::ApiController
    before_action :verify_jwt_token

    def index
        @parent_teacher_meeting = ParentTeacherMeeting.all
    end

    def new
        @parent_teacher_meeting = ParentTeacherMeeting.new
    end


    def show
        @parent_teacher_meeting = ParentTeacherMeeting.find(params[:id])
    end

    def show_all_requested_meetings


        @parent_teacher_meeting = ParentTeacherMeeting.filter_by_student(params[:student_id])
        if @parent_teacher_meeting
            render json:  @parent_teacher_meeting
        else
            render json: {message: "no requests found", status: 400}, status: 400
        end
    end
    def create
        @parent_teacher_meeting = ParentTeacherMeeting.create parent_teacher_meeting_params

        if @parent_teacher_meeting.errors.blank?
                render json: {message: "ptm request added", ptm: @parent_teacher_meeting} , status: :ok

        else
            (render json: { errors: @parent_teacher_meeting.errors.full_messages }, status: :bad_request)
        end
    end


    private
    def set_parent_teacher_meeting
        @parent_teacher_meeting = ParentTeacherMeeting.find(params[:id])
    end

    def parent_teacher_meeting_params
        params.require(:parent_teacher_meeting).permit(
            :parent_id,
            :faculty_id,
            :student_id,
            :subject,
            :description,
            :id
        )
    end
end