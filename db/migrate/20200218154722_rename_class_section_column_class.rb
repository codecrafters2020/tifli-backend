class RenameClassSectionColumnClass < ActiveRecord::Migration[5.2]
  def change
    rename_column :class_sections, :class, :school_class_id
    rename_column :class_sections, :section, :section_name
  end
end
