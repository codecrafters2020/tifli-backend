class AddCompanyIdToCompanyInformations < ActiveRecord::Migration[5.2]
  def change
    add_column :company_informations, :company_id, :string
    add_index :company_informations, :company_id
  end
end
