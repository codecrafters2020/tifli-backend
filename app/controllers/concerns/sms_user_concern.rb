module SmsUserConcern
    extend ActiveSupport::Concern

    def email_authentication? params
        params[:session][:sms_user][:email].match(/^(|(([A-Za-z0-9]+_+)|([A-Za-z0-9]+\-+)|([A-Za-z0-9]+\.+)|([A-Za-z0-9]+\++))*[A-Za-z0-9]+@((\w+\-+)|(\w+\.))*\w{1,63}\.[a-zA-Z]{2,6})$/i)
    end

    def phone_authentication? params
        params[:sms_user][:email].match(/\+(9[976]\d|8[987530]\d|6[987]\d|5[90]\d|42\d|3[875]\d|2[98654321]\d|9[8543210]|8[6421]|6[6543210]|5[87654321]|4[987654310]|3[9643210]|2[70]|7|1)\d{1,14}$/)
    end

    def email_authentication_web? params
        params[:sms_user][:login].match(/^(|(([A-Za-z0-9]+_+)|([A-Za-z0-9]+\-+)|([A-Za-z0-9]+\.+)|([A-Za-z0-9]+\++))*[A-Za-z0-9]+@((\w+\-+)|(\w+\.))*\w{1,63}\.[a-zA-Z]{2,6})$/i)
    end

    def phone_authentication_web? params
        params[:sms_user][:login].match(/\+(9[976]\d|8[987530]\d|6[987]\d|5[90]\d|42\d|3[875]\d|2[98654321]\d|9[8543210]|8[6421]|6[6543210]|5[87654321]|4[987654310]|3[9643210]|2[70]|7|1)\d{1,14}$/)
    end

    #def signout_blacklisted
    #    signed_out = (Devise.sign_out_all_scopes ? sign_out : sign_out(resource_name))
    #    # flash[:notice] = "You're Blacklisted. Please contact Lorryz."
    #    redirect_to new_user_session_path , notice: "Your account is being verified and you will be notified shortly by Lorryz Team. For any enquiries please send email to contact@lorryz.com or call 052 1250605"
    #end

    #def signout_driver
    #    signed_out = (Devise.sign_out_all_scopes ? sign_out : sign_out(resource_name))
    #    # flash[:notice] = "You're Blacklisted. Please contact Lorryz."
    #    redirect_to new_user_session_path , notice: "You are only allowed to access your account from mobile application. For any enquiries please send email to contact@lorryz.com or call 052 1250605"
    #end

    def make_phone_number country_id , phone_number
        #short_name = Country.find_by_id(country_id).try(:short_name)
        #if short_name.present? and phone_number.present?
        #    return  TelephoneNumber.parse(phone_number, short_name).international_number(formatted: true).delete(" ")
        #end
    end

    def phone_number_valid?  country_id , phone_number
        short_name = Country.find_by_id(country_id).try(:short_name)
        return TelephoneNumber.valid?(phone_number, short_name, [:mobile, :fixed_line])
    end

    def admin_or_superadmin? sms_user
        sms_user.admin? or sms_user.superadmin?
    end
end