class ChangeStudentAttendanceToStudentAttendances < ActiveRecord::Migration[5.2]
  def change
    rename_table :student_attendance, :student_attendances
  end
end
