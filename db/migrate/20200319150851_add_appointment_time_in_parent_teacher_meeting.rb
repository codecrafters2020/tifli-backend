class AddAppointmentTimeInParentTeacherMeeting < ActiveRecord::Migration[5.2]
  def change
    remove_column :parent_teacher_meetings, :appointment_date
    add_column :parent_teacher_meetings, :appointment_date, :date
    add_column :parent_teacher_meetings, :appointment_time, :time
  end
end
