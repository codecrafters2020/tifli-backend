class Api::V1::VanDriver::VanDriversController < Api::V1::ApiController

    before_action :set_van , only: [:destroy]

    #before_action :set_shipment, only: [:update_vehicle_status,:report_problem,:payment_received]

    def index
        @van_drivers = VanDriver.all
        respond_to do |format|
            format.html
            format.xlsx {
                start_date = params[:start_date].present? ? params[:start_date].to_date : 100.years.ago.to_date
                end_date = params[:end_date].present? ? params[:end_date].to_date : 100.years.from_now.to_date
                @van_drivers = VanDriver.where(created_at: start_date..end_date)
                render xlsx: 'xlsx_blogs', filename: "all_blogs.xlsx", disposition: 'attachment'
            }
        end
    end

    def new
        @van_driver = VanDriver.new
    end

    def show
        @van_driver = VanDriver.find(params[:id])
    end



    private
    def set_van
        @van_driver = VanDriver.find(params[:id])
    end

    def van_params

    end

    def set_van_driver_link
        @van_driver_link = "active"
    end

end


