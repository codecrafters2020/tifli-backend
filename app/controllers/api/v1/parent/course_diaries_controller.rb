class Api::V1::Parent::CourseDiariesController < Api::V1::ApiController
    before_action :verify_jwt_token

    def index
        @course_diary = CourseDiary.all
    end

    def new
        @course_diary = CourseDiary.new
    end

    def show
        @course_diary = CourseDiary.find(params[:id])
    end

    def get_student_diaries
        @course_diary = CourseDiary.student_diaries(params[:class_section_id], params[:student_id]).get_specific_day_diary(params[:diary_date])
        #@course_diary += CourseDiary.find_by_student_id(params[:student_id])
        if @course_diary
            render json: @course_diary
        else
            render json: { errors: @course_diary.errors.full_messages }, status: :bad_request
        end

    end

    def create
        #
        #@course_diary = CourseDiary.create course_diary_params
        #@course_diary.errors.blank? ? ( render json: {message: "diary added"} , status: :ok ) : (render json: { errors: @course_diary.errors.full_messages }, status: :bad_request)

    end

    private
    def set_course_diary
        @course_diary = CourseDiary.find(params[:id])
    end

    def course_diary_params
        params.require(:course_diary).permit(
            :section_course_id,
            :student_id,
            :diary_info,
            :id,
            :image_url
        )
    end
end