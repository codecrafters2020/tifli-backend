class RemoveCapacityFromStudents < ActiveRecord::Migration[5.2]
  def change
    remove_column :students, :capacity, :integer
  end
end
