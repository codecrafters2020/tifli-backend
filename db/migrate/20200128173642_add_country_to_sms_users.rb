class AddCountryToSmsUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :sms_users, :country_id, :integer
  end
end
