class CreateStudents < ActiveRecord::Migration[5.2]
  def change
    create_table :students do |t|
      t.string :first_name
      t.string :last_name
      t.integer :gender
      t.integer :mobile
      t.string :email
      t.string :enrollment_number
      t.datetime :date_of_birth
      #t.string :van_color, :limit => 8

      t.integer :capacity
      t.integer :parent_id
      t.integer :van_id

      t.timestamps
    end
  end
end
