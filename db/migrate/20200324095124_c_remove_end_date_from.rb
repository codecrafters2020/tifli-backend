class CRemoveEndDateFrom < ActiveRecord::Migration[5.2]
  def change
    remove_column :leave_applications, :end_date
    add_column :leave_applications, :end_date, :date
  end
end
