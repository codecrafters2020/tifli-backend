class SendSMS
  attr_accessor :client
  
  def initialize
    @client = Twilio::REST::Client.new Rails.application.secrets.twilio_account_sid , Rails.application.secrets.twilio_auth_token
  end
 
  def message(to, body)
    begin
      @client.api.account.messages.create( 
        from: Rails.application.secrets.twilio_from_number ,
        to: to, 
        body: body
      )
    rescue Twilio::REST::TwilioError => e
      puts e.message
    end
  end

  class << self
    delegate :message, to: :new
  end
end



