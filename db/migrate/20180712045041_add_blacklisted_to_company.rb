class AddBlacklistedToCompany < ActiveRecord::Migration[5.2]
  def change
    add_column :companies, :blacklisted, :boolean, default: false
    add_column :companies, :safe_for_cash_on_delivery, :boolean, default: false
  end
end
