class LeaveApplication < ApplicationRecord
    audited

    acts_as_paranoid
    belongs_to :student
    has_one_attached :avatar

    scope :filter_by_student, ->(student_id){where("student_id = ?",student_id)}
    scope :filter_by_class_section, ->(class_section_id){joins(:student).merge(Student.filter_by_class_section(class_section_id))}

    scope :get_specific_day_leaves, -> (leave_date){where('? BETWEEN start_date AND end_date', leave_date, leave_date)}

    extend Enumerize
    enumerize :leave_status, in: {
        :pending => 1,
        :approved => 2,
        :disapproved => 3
    }, scope: true, predicates: true

end