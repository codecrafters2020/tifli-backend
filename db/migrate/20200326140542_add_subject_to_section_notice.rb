class AddSubjectToSectionNotice < ActiveRecord::Migration[5.2]
  def change
    add_column :section_notices, :subject, :text
  end
end
