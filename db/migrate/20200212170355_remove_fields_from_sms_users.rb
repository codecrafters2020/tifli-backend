class RemoveFieldsFromSmsUsers < ActiveRecord::Migration[5.2]
  def change
    remove_column :sms_users, :country_id, :integer
  end
end
