class Van < ApplicationRecord
    audited

    extend Enumerize
    acts_as_paranoid

    belongs_to :van_driver


    has_one_attached :avatar
    enumerize :status, in: {
        :start_to_school => 1,
        :end_to_school => 2,
        :start_from_school => 3,
        :end_from_school => 4,
        :free => 5
    }, scope: true, predicates: true

end