class VehicleType < ApplicationRecord
	audited

	belongs_to :country
	has_many :shipments
	has_one :vehicle
	has_one_attached :avatar
end
