class AddBuildingNamesToLocation < ActiveRecord::Migration[5.2]
  def change
    add_column :locations, :pickup_building_name, :string
    add_column :locations, :drop_building_name, :string
  end
end
