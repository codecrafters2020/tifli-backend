class CreateCourses < ActiveRecord::Migration[5.2]
  def change
    create_table :courses do |t|
      t.string :course_name

      t.datetime :deleted_at
      t.timestamps
    end
  end
end
