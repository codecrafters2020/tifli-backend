class CreateClassSections < ActiveRecord::Migration[5.2]
  def change
    create_table :class_sections do |t|


      t.string :class
      t.string :section

      t.integer :faculty_id

      t.timestamps
    end
  end
end
