class AddVanDriverFieldInVans < ActiveRecord::Migration[5.2]
  def change
    add_column :vans, :van_driver_id, :integer
  end
end
