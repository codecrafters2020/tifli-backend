class AddImageUrlToLeaveApplication < ActiveRecord::Migration[5.2]
  def change
    add_column :leave_applications, :image_url, :text
  end
end
