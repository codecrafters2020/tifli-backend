class CreateSchoolClass < ActiveRecord::Migration[5.2]
  def change
    create_table :school_classes do |t|
      t.string :class_name

      t.datetime :deleted_at
      t.timestamps
    end
  end
end
