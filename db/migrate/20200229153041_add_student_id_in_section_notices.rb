class AddStudentIdInSectionNotices < ActiveRecord::Migration[5.2]
  def change
    add_column :section_notices, :student_id, :integer
  end
end
