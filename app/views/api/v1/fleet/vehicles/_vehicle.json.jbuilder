json.extract! vehicle, :id, :registration_number,:expiry_date,:authorization_letter, :insurance_number, :vehicle_type_id, :company_id, :created_at, :updated_at, :latitude, :longitude,:available, :not_available_to,:not_available_from,:documents,:insurance_expiry_date


if vehicle.vehicle_type_id
	json.vehicle_type vehicle.vehicle_type 
end

if vehicle.user_id
	json.driver vehicle.driver 
end
