class Api::V1::Faculty::LeaveApplicationsController < Api::V1::ApiController
    before_action :verify_jwt_token

    def index
        @leave_applicaion = LeaveApplication.all
    end

    def new
        @leave_applicaion = LeaveApplication.new
    end


    def show
        @leave_applicaion = LeaveApplication.find(params[:id])
    end

    def show_all_requested_leaves
        @leave_applicaion = LeaveApplication.filter_by_class_section(params[:class_section_id])
        if @leave_applicaion
            render json:  @leave_applicaion
        else
            render json: {message: "no leave requests found", status: 400}, status: 400
        end
    end

    def create
    end

    def update_leave_status
        @leave_applicaion = LeaveApplication.find params[:leave_application_id]

        if @leave_applicaion.update_attributes(leave_status: params[:leave_status])
            render json: {message: "leave status updated", status: :ok}
        else
            (render json: { errors: @leave_applicaion.errors.full_messages }, status: :bad_request)
        end
    end

    def get_leaves_by_date
        @leave_applicaion = LeaveApplication.get_specific_day_leaves(params[:leave_date])
        if @leave_applicaion
            render json: @leave_applicaion
        end
    end

    private
    def set_leave_application
        @leave_applicaion = LeaveApplication.find(params[:id])
    end

    def leave_application_params
        params.require(:leave_application).permit(
            :student_id,
            :subject,
            :leave_reason,
            :start_date,
            :end_date,
            :leave_status,
            :image_url,
            :id
        )
    end
end