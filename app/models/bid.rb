class Bid < ApplicationRecord
	audited
	belongs_to :shipment
	belongs_to :company

	before_save :set_status
	scope :active, -> {where(status: :open)}
	scope :accepted, -> {where(status: :accepted)}
	scope :rejected, -> {where(status: :rejected)}
	scope :accept_bid, -> (bid) {where.not(id: bid.id).update_all(:status => :rejected)}

	# scope :fleet_active, -> (company_id) {includes(:bids).where(bids: {company_id: company_id}, state: "posted")}
	extend Enumerize

	enumerize :status, in: {:open => 1, :accepted => 2,:rejected => 3}, scope: true, predicates: true 
	
	default_scope { includes(:company).order("companies.verified desc", "companies.featured desc") }

	scope :available_bids,-> { includes(:company).order("companies.verified DESC").where.not(status: :rejected) }
	scope :best_bid ,-> { unscope(:order).order(amount: :asc).first }

	validates :amount, presence: :true ,numericality: { greater_than: 0 , message: I18n.t("greater_than_zero")}

	def set_status
		self.status = :open unless self.status
	end

end
