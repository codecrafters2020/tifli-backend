class Fleet::BidsController < FleetController
  before_action :set_bid, only: [:show, :edit, :update, :destroy,:accept,:reject]

  # GET /bids
  # GET /bids.json
  def index
    @bids = Bid.all
  end

  # GET /bids/1
  # GET /bids/1.json
  def show
  end

  # GET /bids/new
  def new
    @previous_bid = shipment.bids.where(company_id: current_user.company.id).try(:last)
    @bid = shipment.bids.new
    @location_amount = Location.find(@shipment.location_id).rate.to_f rescue nil if @shipment.company.is_contractual?
    # @bid = Bid.new
  end

  # GET /bids/1/edit
  def edit
  end

  # POST /bids
  # POST /bids.json
  def create
    shipment.bids.where(company_id: current_user.company_id).destroy_all rescue nil
    @bid = Bid.new(bid_params)
    @bid.company_id = current_user.company.id
    respond_to do |format|
      if @bid.save
        format.html { redirect_to active_fleet_shipments_path , notice: 'Bid was successfully created.' }
        # format.json { render :show, status: :created, location: @bid }
      else
        flash[:error] = @bid.errors.full_messages
        format.html { redirect_to new_fleet_shipment_bid_path(shipment_id: bid_params["shipment_id"])}
        # format.json { render json: @bid.errors, status: :unprocessable_entity }
      end
    end
  end
  def bid_details
    @shipment = shipment
    @bid = @shipment.bids.new
    calculate_income params
    respond_to do |f|
      f.js
    end
  end

  # PATCH/PUT /bids/1
  # PATCH/PUT /bids/1.json
  def update
    respond_to do |format|
      if @bid.update(bid_params)
        format.html { redirect_to @bid, notice: 'Bid was successfully updated.' }
        format.json { render :show, status: :ok, location: @bid }
      else
        format.html { render :edit }
        format.json { render json: @bid.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /bids/1
  # DELETE /bids/1.json
  def destroy
    @bid.destroy
    respond_to do |format|
      format.html { redirect_to bids_url, notice: 'Bid was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  def calculate_income params
    set_bid_amount
    commission = @shipment.country.commision
    @lorryz_share = @shipment.lorryz_share_calculation_for_given_amount @bid_amount #.to_f / 100) * commission.to_f
    @your_income = @bid_amount - @lorryz_share rescue 0.0

    @detention_amount = @shipment.process == "prorate" ? @shipment.detention.to_f :  params[:bid][:detention].to_f
    @lorryz_detention_income = @shipment.lorryz_share_calculation_for_given_amount @detention_amount
    @your_detention_income = @detention_amount - @lorryz_detention_income rescue 0.0
  end
  private
  # Use callbacks to share common setup or constraints between actions.
  def set_bid
    @bid = Bid.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def bid_params
    params.require(:bid).permit(:company_id, :amount, :status, :detention, :shipment_id)
  end
  def set_bid_amount
    if @shipment.location.present? or @shipment.process == "prorate"
      @bid_amount = @shipment.amount
    else
      @bid_amount = params[:bid][:amount].to_f
    end
  end
  # def company
  #   company = Company.find(params[:company_id])
  # end
  def shipment
    # company.shipments.find_by_id(params[:shipment_id])
    @shipment = Shipment.find_by_id(params[:shipment_id])
  end
end
