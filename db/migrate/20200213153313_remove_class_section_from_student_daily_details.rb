class RemoveClassSectionFromStudentDailyDetails < ActiveRecord::Migration[5.2]
  def change
    remove_column :student_daily_details, :class_section_id, :integer
  end
end
