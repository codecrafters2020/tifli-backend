$(document).on 'turbolinks:load', ->
  $('[data-provider="summernote"]').each ->
    $(this).summernote
      height: 500
  
  if $('.note-btn-group.btn-group.note-view .btn-fullscreen').length > 0
    $('.note-btn-group.btn-group.note-view .btn-fullscreen').remove();