class CourseDiarySerializer < ActiveModel::Serializer
    attributes :id, :diary_info, :section_course, :student, :image_url

    belongs_to :section_course
    belongs_to :student, optional: true
end