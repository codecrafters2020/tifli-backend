class CompleteClassSectionSerializer < ActiveModel::Serializer
    attributes :id, :section, :school_class, :faculty, :students, :section_courses

    belongs_to :faculty
    belongs_to :school_class
    belongs_to :section
    has_many :students
    has_many :section_courses

end