class ParentTeacherMeetingSerializer < ActiveModel::Serializer
    attributes :id, :subject, :description, :parent, :faculty, :faculty_name, :student_id, :student_name, :parent_teacher_meeting_comments, :created_at, :appointment_date, :appointment_time


    belongs_to :faculty
    belongs_to :parent

    #has_many :parent_teacher_meeting_comments

    def student_name
        if object && object.student
            object.student.first_name + object.student.last_name
        end
    end

    def faculty_name
        if object && object.faculty
            object.faculty.sms_user.first_name + object.faculty.sms_user.last_name
        end
    end
end