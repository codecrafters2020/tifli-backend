class SmsUser < ApplicationRecord
  audited

  acts_as_paranoid
  validates :email, :uniqueness => {:allow_blank => true}
  attr_writer :login

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  validates :mobile, presence: true, uniqueness: true

  extend Enumerize
  #include PhoneVerificationConcern
  enumerize :role, in: {
      :superadmin => 1,
      :admin => 2,
      :faculty => 3,
      :parent => 4,
      :driver=> 5 }, scope: true, predicates: true

  #belongs_to :country, optional: true
  has_many :login_devices

  has_one :van_driver
  accepts_nested_attributes_for :van_driver
  has_one :van, :through => :van_driver

  has_one :faculty
  accepts_nested_attributes_for :faculty
  has_one :class_section, :through => :faculty
  accepts_nested_attributes_for :class_section

  has_one :parent
  accepts_nested_attributes_for :parent

  has_many :students, :through => :parent



  scope :drivers, -> {where(role: :driver)}
  scope :faculty ,-> {where(role: :faculty)}
  scope :guardian ,-> {where(role: :parent)}
  scope :admins ,-> {where(role: :admin)}
  scope :superadmins ,-> {where(role: :superadmin)}
  scope :filter_by_user, -> (user_id) { where('sms_users.id =?',user_id) }

  def self.fetch_user token
    decoded_token = JWT.decode(token, Rails.application.secrets.secret_key_base)
    SmsUser.find(decoded_token[0]["user_id"])
  end

  def self.find_by_email_or_phone(params)

    find_by(mobile: params[:sms_user][:mobile]) || find_by(email: params[:sms_user][:email])

  end

  def login=(login)
    @login = self.email
  end


  def email_required?
    false
  end

  def login
    @login || self.mobile || self.email
  end

  def self.find_for_database_authentication(warden_conditions)
    conditions = warden_conditions.dup
    if login = conditions.delete(:login)
      where(conditions.to_h).where(["lower(mobile) = :value OR lower(email) = :value", { :value => login.downcase }]).first
    elsif conditions.has_key?(:mobile) || conditions.has_key?(:email)
      where(conditions.to_h).first
    end
  end
  def full_name
    "#{first_name} #{last_name}"
  end

end
