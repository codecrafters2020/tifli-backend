class Api::V1::Parent::LeaveApplicationsController < Api::V1::ApiController
    before_action :verify_jwt_token

    def index
        @leave_applicaion = LeaveApplication.all
    end

    def new
        @leave_applicaion = LeaveApplication.new
    end


    def show
        @leave_applicaion = LeaveApplication.find(params[:id])
    end

    def create
        @leave_applicaion = LeaveApplication.create leave_application_params

        if @leave_applicaion.errors.blank?
            render json: {message: "leave request added", ptm: @leave_applicaion} , status: :ok

        else
            (render json: { errors: @leave_applicaion.errors.full_messages }, status: :bad_request)
        end
    end

    def show_all_requested_leaves

        @leave_applicaion = LeaveApplication.filter_by_student(params[:student_id])
        if @leave_applicaion
            render json:  @leave_applicaion
        else
            render json: {message: "no leave requests found", status: 400}, status: 400
        end
    end

    private
    def set_leave_application
        @leave_applicaion = LeaveApplication.find(params[:id])
    end

    def leave_application_params
        params.require(:leave_application).permit(
            :student_id,
            :subject,
            :leave_reason,
            :start_date,
            :end_date,
            :leave_status,
            :image_url,
            :id
        )
    end
end