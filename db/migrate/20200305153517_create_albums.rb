class CreateAlbums < ActiveRecord::Migration[5.2]
  def change
    create_table :albums do |t|
      t.string :album_name
      t.string :description

      t.boolean :is_public, default: false

      t.datetime :deleted_at
      t.timestamps
    end
  end
end
