class DashboardController < ApplicationController
  before_action :authenticate_user! ,except: [:home, :share_iphone, :share_android, :contact , :terms_and_conditions, :privacy_policy,:home_shipment]

  def index
    user_signed_in? ? (send(current_user.role) rescue rescue_redirection) : (rescue_redirection)
  end  



  def home

  end

  def home_shipment
    
    if user_signed_in?
      send("#{current_user.role}_redirect")
    else
      cookies[:shipment] = params if params 
      redirect_to new_user_session_path,notice: "You need to signin before creating shipment"  
    end
    


  end

  def share_iphone
    puts params[:phone_no]
    begin
      dispathcer = SendSMS.new
      message = dispathcer.message(params[:phone_no],"https://itunes.apple.com/pk/app/lorryz/id1451176427?mt=8&ign-mpt=uo%3D4 \nTeam Lorryz")
    rescue
    end
    redirect_to root_path, notice: "Your message has been delivered"
  end

  def share_android
    begin
      dispathcer = SendSMS.new
      message = dispathcer.message(params[:phone_no],"https://play.google.com/store/apps/details?id=com.LORRYZ \nTeam Lorryz")
    rescue
    end
    redirect_to root_path, notice: "Your message has been delivered"
  end

  def contact
    @user = params[:user]
    if(verify_recaptcha())
        begin
          UserMailer.send_customer_info(@user).deliver
        rescue StandardError => e

        end
        redirect_to root_path, notice: "We have received your information successfully. We will get back to you very soon. Thanks"  
    else      
      @error = true
    end
    
  end

  def terms_and_conditions 
    @page=Page.find_by(page_type: 'terms_and_conditions')
  end
  
  def privacy_policy
    @page=Page.find_by(page_type: 'privacy_policy')
  end

  private

    def cargo_owner
      cookies[:shipment].present? ? (redirect_to(new_cargo_shipment_path)) : (redirect_to cargo_root_path)
    end

    def fleet_owner
      redirect_to fleet_root_path
    end
      

    %w(admin superadmin).each do |method_name|
      define_method method_name do 
        redirect_to admin_root_path
      end
    end


    %w(admin_redirect superadmin_redirect fleet_owner_redirect).each do |method_name|
      define_method method_name do
        redirect_to_dashboard 
      end
    end  

    def rescue_redirection
      redirect_to dashboard_index_path
    end


    def cargo_owner_redirect
      cookies[:shipment] = params if params
      redirect_to new_cargo_shipment_path
    end

    def redirect_to_dashboard
      redirect_to home_dashboard_index_path,notice: "You are unauthorized to create shipment"
    end  

end
