class CreateVans < ActiveRecord::Migration[5.2]
  def change
    create_table :vans do |t|
      t.string :van_name
      t.string :van_number, :limit => 8
      t.string :van_color

      t.integer :capacity
      t.integer :driver_id

      t.timestamps
    end
  end
end
