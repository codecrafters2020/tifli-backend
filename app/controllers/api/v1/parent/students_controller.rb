class Api::V1::Parent::StudentsController < Api::V1::ApiController

    before_action :verify_jwt_token
    #before_action :set_student , only: [:update_van_location]
    #
    #before_action :set_shipment, only: [:update_vehicle_status,:report_problem,:payment_received]

    def index
        @student = Student.all
    end

    def new
        @student = Student.new
    end

    def show
        @student = Student.find(params[:id])
    end


    def get_student_attendance_details
        @student_attendance = StudentAttendance.filter_by_student(params[:student_id])

        #puts "student_attendance: #{@student_attendance}"

        if @student_attendance

            @student_attendance_details = get_attendance_details(@student_attendance)

            respond_to do |format|
                format.json { render json: {student_attendance_details: @student_attendance_details}, status: :ok }
            end
        else
            render json: {message: "no records of attendance for this student", status: 400}, status: 400
        end
    end

    def get_detailed_student_attendance
        @student_attendance = StudentAttendance.filter_by_attendance_student(params[:from_date], params[:to_date], params[:student_id])

        if @student_attendance

            respond_to do |format|
                format.json { render json: {student_attendance_details: @student_attendance}, status: :ok }
            end
        else
            render json: {message: "no records of attendance for this student in this date range", status: 400}, status: 400
        end
    end

    def get_student_van
        @van = Van.find(params[:van_id])
        if @van
            render json:  @van
        else
            render json: {message: "no van found", status: 400}, status: 400
        end
    end
    def get_van_current_location
        @van = Van.find(params[:van_id])
        if @van
            render json:  {latitude: @van.latitude, longitude:@van.longitude}, status: :ok
        else
            render json: {message: "no van found", status: 400}, status: 400
        end
    end


    private
    def set_students
        @students = Student.find(params[:id])
    end

    #def student_attendance_params
    #    params.require(:student_attendance).map do |p|
    #        p.permit(
    #            :student_id,
    #            :id,
    #            :attendance
    #        )
    #    end
    #
    #    #params.require(:student_attendance).permit()
    #end

    def get_attendance_details(student_attendances)
        @present = 0
        @absent = 0
        @leaves = 0
        student_attendances.each do |student_day_record|
            if student_day_record.attendance.absent?
                @absent += 1
            elsif student_day_record.attendance.leave?
                @leaves += 1
            elsif student_day_record.attendance.present?
                @present += 1
            end
        end
        return [@present, @absent, @leaves]
    end


end