class StudentSerializer < ActiveModel::Serializer
    attributes :id, :first_name, :last_name, :gender, :mobile, :enrollment_number, :date_of_birth, :class_section, :van_id

    #belongs_to :parent
    #belongs_to :van
    belongs_to :class_section
    def van_id
        if object && object.van
            object.van.id
        end
    end
    #def class_section
    #    if object
    #        object.class_section
    #    end
    #end
end