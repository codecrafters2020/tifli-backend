class ApplicationMailer < ActionMailer::Base
  default from: 'Lorryz <no-reply@lorryz.com>'
  layout 'mailer'
end
