class Api::V1::Faculty::SectionCoursesController < Api::V1::ApiController

    before_action :verify_jwt_token
    #before_action :set_student , only: [:update_van_location]
    #
    #before_action :set_shipment, only: [:update_vehicle_status,:report_problem,:payment_received]

    def index
        @section_course = SectionCourse.all
    end

    def new
        @section_course = SectionCourse.new
    end

    def show
        @section_course = SectionCourse.find(params[:id])
    end

    def get_faculty_teached_section
        #@section_courses = SectionCourse.filter_faculty params[:faculty_id]

        @class_section = ClassSection.teached_class_sections params[:faculty_id], params[:school_class_id]
        if @class_section


            render json: @class_section
        else
            render json: { errors: @class_section.errors.full_messages }, status: :bad_request
        end


        #respond_to do |format|
        #    format.json { render json: {section_courses: section_courses}, status: :ok }
        #end
    end

    def get_faculty_teached_class
        #@section_courses = SectionCourse.filter_faculty params[:faculty_id]

        @class = ClassSection.select(:school_class_id).distinct.teached_classes params[:faculty_id]
        if @class


            render json: {classes: @class}, status: :ok
        else
            render json: { errors: @class.errors.full_messages }, status: :bad_request
        end


        #respond_to do |format|
        #    format.json { render json: {section_courses: section_courses}, status: :ok }
        #end
    end

    def get_section_courses
        #@class_section = ClassSection.find(params[:id])
        @section_course = SectionCourse.filter_faculty( params[:section_id], params[:faculty_id])
        if @section_course
            render json:  @section_course
        else
            render json: { errors: @section_course.errors.full_messages }, status: :bad_request
        end
    end



    private
    def set_section_course
         @section_course = SectionCourse.find(params[:id])
    end

    def section_course_params
        params.require(:section_course).permit(
                :class_section_id,
                :faculty_id,
                :course_id,
                :id
            )
        #params.require(:student_attendance).permit()
    end
    def class_section
        ClassSection.find(params[:class_section_id])
    end
end



