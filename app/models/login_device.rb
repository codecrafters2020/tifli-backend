class LoginDevice < ApplicationRecord
    audited

    acts_as_paranoid

    belongs_to :sms_user

end
