class Api::V1::Faculty::FacultyTimetablesController < Api::V1::ApiController
    before_action :verify_jwt_token

    def index
        @faculty_timetable = FacultyTimetable.all
    end

    def new
        @faculty_timetable = FacultyTimetable.new
    end


    def show
        @faculty_timetable = FacultyTimetable.find(params[:id])
    end

    def get_faculty_timetable
        @faculty_timetable = FacultyTimetable.find(params[:faculty_id])
        if @faculty_timetable
            render json: @faculty_timetable
        else
            render json: {message: "no records of timetable for this faculty", status: 400}, status: 400
        end
    end

    def create
    end

    private
    def set_faculty_timetable
        @faculty_timetable = FacultyTimetable.find(params[:id])
    end

end