class CreateLoginDevices < ActiveRecord::Migration[5.2]
  def change
    create_table :login_devices do |t|
      t.string :device_id
      t.string :os

      t.timestamps
    end
  end
end
