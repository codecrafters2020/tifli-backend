class CreateParentTeacherMeetingComments < ActiveRecord::Migration[5.2]
  def change
    create_table :parent_teacher_meeting_comments do |t|
      t.integer :parent_teacher_meeting_id
      t.integer :sent_by
      t.string :message

      t.datetime :deleted_at
      t.timestamps
    end
  end
end
