class ShipmentMailer < ApplicationMailer

	default from: 'Lorryz <no-reply@lorryz.com>'

	def change_destination_mail_to_admin(shipment, old_destination, new_destination)
		@shipment = shipment
		@old_destination = old_destination
		@new_destination = new_destination
		mail(to: "contact@lorryz.com", subject: "Request received for change in destination for #{@shipment.try(:id)}")
	end

	def change_destination_approved_mail(shipment, old_destination)
		@shipment = shipment
		@old_destination = old_destination
		shipment_users
		subject = "Lorryz - Shipment Change Destination Request Approved"
		mail(to: [@cargo_owner_email, @fleet_owner_email], subject: subject) if @cargo_owner_email.present? or @fleet_owner_email.present?
		# mail(to: "sohail.khalil@virtual-force.com", subject: "Request received for change in destination for #{@shipment.try(:id)}")
	end

	def shipment_canceled shipment_id
		@shipment = Shipment.find(shipment_id)
		shipment_users
		subject = "Lorryz - Shipment Cancelled"
		mail(to: ["contact@lorryz.com", @cargo_owner_email, @fleet_owner_email], subject: subject)
	end

	def shipment_rating shipment_id, receiver_id
		@shipment = Shipment.find(shipment_id)
		@email = User.find(receiver_id).email
		subject = "Lorryz - Your Shipment has been Rated."
		mail(to: @email , subject: subject)
	end

	private

	def shipment_users
		@fleet_owner_email = @shipment.fleet.fleet_owner.email rescue ""
		@cargo_owner_email = @shipment.company.cargo_owner.email rescue ""
	end

end
