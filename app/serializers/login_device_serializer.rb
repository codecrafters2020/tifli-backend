class LoginDeviceSerializer < ActiveModel::Serializer
    attributes :id, :os, :device_id, :sms_user

    belongs_to :sms_user
end