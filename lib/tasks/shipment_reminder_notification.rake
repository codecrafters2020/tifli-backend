desc 'Notification to Driver for the reminder of Shipment'
task shipment_reminder_notification: :environment do

  puts "Shipment reminder notification"

  shipments = Shipment.vehicle_assigned

  shipments.each do |shipment|
    hours = ((shipment.pickup_time - Time.zone.now )/3600).round
    if hours >=20 and hours <= 24
      shipment.notify_driver_for_shipment_reminder
    end
  end

end