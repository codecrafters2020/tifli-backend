class Api::V1::Faculty::PicturesController < Api::V1::ApiController
    before_action :verify_jwt_token

    require 'date'
    def index
        @picture = Picture.all
    end

    def new
        @picture = Picture.new
    end


    def show
        @picture = Picture.find(params[:id])
    end

    def create
        params[:images].each do |picture|

            @picture = Picture.create(album_id: params[:album_id], image_url: picture, faculty_id: params[:faculty_id] )
        end
        @picture.errors.blank? ? ( render json: {message: "pictures added"} , status: :ok ) : (render json: { errors: @picture.errors.full_messages }, status: :bad_request)

    end

    def delete_pictures
        #str_arr = params[:picture_ids].split(',')
        #int_array = str_arr.map!(&:to_i)

        @picture = Picture.filter_by_id(params[:picture_ids])
        @picture.update_all(deleted_at: params[:current_time])

        render json: {message: "picture deleted"} , status: :ok

    end

    private
    def set_picture
        @picture = Picture.find(params[:id])
    end

    def picture_params
        params.require(:picture).map do |p|
            p.permit(
                :picture_name,
                :description,
                :avatar,
                :album_id,
                :id
            )
        end
    end
end