class ChangeStudentDailyDetailsToStudentAttendance < ActiveRecord::Migration[5.2]
  def change
    rename_table :student_daily_details, :student_attendance
  end
end
