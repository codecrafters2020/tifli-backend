class SmsUserSerializer < ActiveModel::Serializer
    attributes :id ,:first_name,:last_name,:email,:mobile,:role, :information, :auth_token, :students, :section_students

    has_one :van, optional: true
    has_one :class_section, optional: true
    has_many :students

    def information
        if object && object.role.driver?
            object.van_driver
        elsif object && object.role.faculty?
            object.faculty
        elsif object && object.role.parent?
            object.parent
        end
    end

    def section_students
        if object.role.faculty?
            if object.class_section
                object.faculty.class_section.students
            end
        end
    end

    def auth_token
        AuthToken.issue_token({ user_id: object.id }) if object
    end
end