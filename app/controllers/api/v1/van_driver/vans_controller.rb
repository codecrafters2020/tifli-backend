class Api::V1::VanDriver::VansController < Api::V1::ApiController

    before_action :set_van , only: [:update_van_location]

    #before_action :set_shipment, only: [:update_vehicle_status,:report_problem,:payment_received]

    def index
        @van = Van.all
    end

    def new
        @van = Van.new
    end

    def show
        @van = Van.find(params[:id])
        render json: @van
    end

    def update_van_location
        if @van.update(van_params)
            render json: { message: "lat lng updated" }, status: :ok
        else
            render json: { errors: @van.errors.full_messages }, status: :bad_request
        end
    end

    def update_status
        @van = Van.find params[:id]
        @van.update_attributes(status: params[:status])
    end

    private
    def set_van
        @van = Van.find(params[:id])
    end

    def van_params
        params.require(:van).permit(:van_driver_id,:id, :latitude, :longitude, :status, :capacity, :van_registration_number, :van_color)
    end

    def set_van_driver_link
        @van_link = "active"
    end
end


