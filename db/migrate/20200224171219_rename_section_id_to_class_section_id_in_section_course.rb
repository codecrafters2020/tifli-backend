class RenameSectionIdToClassSectionIdInSectionCourse < ActiveRecord::Migration[5.2]
  def change
    rename_column :section_courses, :section_id, :class_section_id
  end
end
