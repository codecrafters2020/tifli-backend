class AddImageUrlIntoCourseDiaries < ActiveRecord::Migration[5.2]
  def change
    add_column :course_diaries, :image_url, :text
  end
end
