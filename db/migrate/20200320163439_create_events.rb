class CreateEvents < ActiveRecord::Migration[5.2]
  def change
    create_table :events do |t|
      t.string :event_name
      t.string :description
      t.date :event_date
      t.text :image_url

      t.datetime :deleted_at
      t.timestamps
    end
  end
end
