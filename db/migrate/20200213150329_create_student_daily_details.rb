class CreateStudentDailyDetails < ActiveRecord::Migration[5.2]
  def change
    create_table :student_daily_details do |t|

      t.datetime :van_pickup_time
      t.datetime :van_dropoff_time
      t.datetime :school_check_in_time
      t.datetime :school_check_out_time

      t.boolean :attendance, default: true

      t.integer :student_id
      t.integer :class_section_id

      t.timestamps
    end
  end
end
