class Event < ApplicationRecord
    audited

    acts_as_paranoid

    has_one_attached :avatar
end