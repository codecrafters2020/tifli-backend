class CreateVanDrivers < ActiveRecord::Migration[5.2]
  def change
    create_table :van_drivers do |t|
      t.string :liscense_number
      t.string :NIC, :limit => 13

      t.integer :user_id

      t.timestamps
    end
  end
end
