class RemoveColumnFromVans < ActiveRecord::Migration[5.2]
  def change
    remove_column :vans, :van_name, :string
    rename_column :vans, :van_number, :van_registration_number
  end
end
