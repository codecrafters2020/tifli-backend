class ParentTeacherMeeting < ApplicationRecord
    audited
    extend Enumerize
    acts_as_paranoid

    belongs_to :faculty
    belongs_to :parent
    belongs_to :student
    has_many :parent_teacher_meeting_comments
    scope :filter_by_student, ->(student_id){where("student_id = ?",student_id)}
    scope :filter_by_faculty, ->(faculty_id){where("faculty_id = ?",faculty_id)}

    #enumerize :ptm_type, in: {
    #    :physical_meeting => 1,
    #    :virtual_messaging => 2
    #}

end