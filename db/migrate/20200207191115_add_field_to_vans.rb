class AddFieldToVans < ActiveRecord::Migration[5.2]
  def change
    add_column :vans, :latitude, :float
    add_column :vans, :longitude, :float

    remove_column :vans, :driver_id, :integer
  end
end
