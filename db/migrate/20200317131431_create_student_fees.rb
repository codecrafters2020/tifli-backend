class CreateStudentFees < ActiveRecord::Migration[5.2]
  def change
    create_table :student_fees do |t|

      t.integer :student_id
      t.date :fees_month
      t.date :due_date
      t.float :amount


      t.datetime :deleted_at
      t.timestamps
    end
  end
end
