class ParentTeacherMeetingCommentSerializer < ActiveModel::Serializer
    attributes :id, :sent_by, :message,:parent_teacher_meeting

    belongs_to :parent_teacher_meeting

end