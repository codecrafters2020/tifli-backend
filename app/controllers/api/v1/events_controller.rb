class Api::V1::EventsController < Api::V1::ApiController
    before_action :verify_jwt_token

    def index
        @event = Event.all
    end

    def new
        @event = Event.new
    end


    def show
        @event = Event.find(params[:id])
    end
    def get_all_events

        @event = Event.all

        if @event
            render json: @event
        else
            render json: { errors: @event.errors.full_messages }, status: :bad_request
        end

    end

    def create
    end


    private
    def set_event
        @event = Event.find(params[:id])
    end
end