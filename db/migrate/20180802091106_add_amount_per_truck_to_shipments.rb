class AddAmountPerTruckToShipments < ActiveRecord::Migration[5.2]
  def change
    add_column :shipments, :amount_per_vehicle, :decimal, default: 0
    add_column :shipments, :detention_per_vehicle, :decimal, default: 0
  end
end
