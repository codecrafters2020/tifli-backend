class SchoolClass < ApplicationRecord
    audited

    acts_as_paranoid

    has_many :class_sections

end