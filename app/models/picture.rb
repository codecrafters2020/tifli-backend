class Picture < ApplicationRecord
    audited

    acts_as_paranoid

    belongs_to :album
    belongs_to :faculty

    has_one_attached :avatar

    scope :filter_by_id, -> (picture_id) { where('id IN (?)', picture_id) }
    scope :filter_by_album, -> (album_id){where('album_id = ?', album_id)}
end