class StudentAttendance < ApplicationRecord
    audited

    extend Enumerize
    acts_as_paranoid

    belongs_to :student

    scope :filter_by_attendance_student, -> (from_date, to_date, student_id) { where("student_id = ? and created_at between ? and ?  ", student_id, from_date.to_date, to_date.to_date) }
    scope :filter_by_student, ->(student_id){where("student_id = ?",student_id)}
    enumerize :attendance, in: {
        :absent => 1,
        :present => 2,
        :leave => 3
    }
end